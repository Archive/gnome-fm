/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* icon-container-test.c - Test program for the GnomeIconContainer widget.

   Copyright (C) 1999 Free Software Foundation

   The Gnome Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   The Gnome Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with the Gnome Library; see the file COPYING.LIB.  If not,
   write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.

   Author: Ettore Perazzoli <ettore@comm2000.it>
*/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gnome.h>

#include <sys/time.h>

#include "gnome-icon-container.h"

static GdkImlibImage *icon_image;
static gboolean exit_immediately = FALSE;


static void
line_up_callback (GtkWidget *widget,
		  gpointer data)
{
	gnome_icon_container_line_up (GNOME_ICON_CONTAINER (data));
}

static void
relayout_callback (GtkWidget *widget,
		   gpointer data)
{
	gnome_icon_container_relayout (GNOME_ICON_CONTAINER (data));
}

static void
add_icon_callback (GtkWidget *widget,
		   gpointer data)
{
	static gint count = 0;

	gnome_icon_container_add_imlib_auto (GNOME_ICON_CONTAINER (data),
					icon_image,
					g_strdup_printf ("New icon %d",
							 ++count),
					"New icon");
}

static void
unselect_all_callback (GtkWidget *widget,
		       gpointer data)
{
	gnome_icon_container_unselect_all (GNOME_ICON_CONTAINER (data));
}

static void
select_all_callback (GtkWidget *widget,
		     gpointer data)
{
	gnome_icon_container_select_all (GNOME_ICON_CONTAINER (data));
}

static void
set_small_icon_container_callback (GtkWidget *widget,
			      gpointer data)
{
	gnome_icon_container_set_icon_mode (GNOME_ICON_CONTAINER (data),
				       GNOME_ICON_CONTAINER_SMALL_ICONS);
}

static void
set_normal_icon_container_callback (GtkWidget *widget,
			       gpointer data)
{
	gnome_icon_container_set_icon_mode (GNOME_ICON_CONTAINER (data),
				       GNOME_ICON_CONTAINER_NORMAL_ICONS);
}


static GnomeUIInfo icon_container_submenu_items[] = {
	GNOMEUIINFO_ITEM_NONE (N_("Normal icons"),
			       N_("Normal icon mode"),
			       set_normal_icon_container_callback),
	GNOMEUIINFO_ITEM_NONE (N_("Small icons"),
			       N_("Small icon mode"),
			       set_small_icon_container_callback),
	GNOMEUIINFO_END
};

static GnomeUIInfo popup_items[] = {
	GNOMEUIINFO_SUBTREE ("Icon mode", icon_container_submenu_items),
	GNOMEUIINFO_ITEM_NONE (N_("Select all"),
			       N_("Select all icons"),
			       select_all_callback),
	GNOMEUIINFO_ITEM_NONE (N_("Unselect all"),
			       N_("Unselect all icons"),
			       unselect_all_callback),
	GNOMEUIINFO_SEPARATOR,
	GNOMEUIINFO_ITEM_NONE (N_("Line up icons"),
			       N_("Line up icons"),
			       line_up_callback),
	GNOMEUIINFO_ITEM_NONE (N_("Re-layout icons"),
			       N_("Re-layout icons"),
			       relayout_callback),
	GNOMEUIINFO_ITEM_NONE (N_("Add icon"),
			       N_("Add icon"),
			       add_icon_callback),
	GNOMEUIINFO_END
};


static void
delete_callback (GtkObject *object, gpointer data)
{
	gtk_main_quit ();
}

static void
button_press_callback (GtkWidget *widget,
		       GdkEventButton *event,
		       gpointer data)
{
	static GtkWidget *menu = NULL;

	if (event->button != 3)
		return;

	if (menu == NULL)
		menu = gnome_popup_menu_new (popup_items);

	gnome_popup_menu_do_popup_modal (menu, NULL, NULL, event, data);
}

static void
add_icons (GnomeIconContainer *icon_container,
	   GdkImlibImage *image,
	   guint num_icons)
{
	struct timeval tv;
	gdouble t1, t2;
	guint i;

	gettimeofday (&tv, NULL);
	t1 = (gdouble) tv.tv_sec + ((gdouble) tv.tv_usec / 1000000.0);

	for (i = 0; i < num_icons; i++) {
		gchar *text;

		text = g_strdup_printf ("Icon number %d", i);

		gnome_icon_container_add_imlib_auto (GNOME_ICON_CONTAINER (icon_container),
						image, text, text);

#if 0
		if (i % 10 == 0) {
			while (gtk_events_pending ())
				gtk_main_iteration ();
		}
#endif
	}

	gettimeofday (&tv, NULL);
	t2 = (gdouble) tv.tv_sec + ((gdouble) tv.tv_usec / 1000000.0);

	printf ("Adding %d icons took %f seconds, %f icons/sec.\n",
		num_icons, t2 - t1, (gdouble) num_icons / (t2 - t1));

	if (exit_immediately)
		exit (1);
}

static void
selection_changed_callback (GnomeIconContainer *container,
			    gpointer data)
{
	GList *selection, *p;

	printf ("\n*** Selection changed!\n");

	selection = gnome_icon_container_get_selection (container);
	if (selection == NULL) {
		printf ("No icons selected.\n");
		return;
	}

	printf ("Selected files:\n");
	for (p = selection; p != NULL; p = p->next)
		printf ("\t%s\n", (gchar *) p->data);

	g_list_free (p);
}

static void
create_icon_container (guint num_icons)
{
	GtkWidget *window;
	GtkWidget *scrolled_window;
	GtkWidget *icon_container;

	icon_image = gdk_imlib_load_image (ICONDIR "/i-core.png");
	if (icon_image == NULL)
		g_error ("Cannot load `i-directory.png'");

	window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	gtk_window_set_default_size (GTK_WINDOW (window), 640, 400);
	gtk_signal_connect (GTK_OBJECT (window),
			    "delete_event", delete_callback, NULL);

	scrolled_window = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled_window),
					GTK_POLICY_AUTOMATIC,
					GTK_POLICY_ALWAYS);
	gtk_container_add (GTK_CONTAINER (window), scrolled_window);

	icon_container = gnome_icon_container_new ();
	gtk_container_add (GTK_CONTAINER (scrolled_window), icon_container);

	gtk_signal_connect (GTK_OBJECT (icon_container), "selection_changed",
			    GTK_SIGNAL_FUNC (selection_changed_callback),
			    NULL);

	gtk_widget_show (icon_container);
	gtk_widget_show (scrolled_window);
	gtk_widget_show (window);

	add_icons (GNOME_ICON_CONTAINER (icon_container), icon_image, num_icons);

	gtk_widget_grab_focus (icon_container);
	gtk_signal_connect (GTK_OBJECT (icon_container), "button_press",
			    GTK_SIGNAL_FUNC (button_press_callback),
			    icon_container);
}

int
main (int argc, char **argv)
{
	guint num_icons;

	gnome_init ("IconContainerTest", "0.0", argc, argv);

	if (argc >= 2)
		num_icons = atoi (argv[1]);
	else
		num_icons = 10;

	if (argc > 2)
		exit_immediately = TRUE;

	create_icon_container (num_icons);

	gtk_main ();

	return 0;
}
