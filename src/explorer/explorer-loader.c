/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* explorer-loader.c
 *
 * Copyright (C) 1999  Free Software Foundaton
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Author: Ettore Perazzoli
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gnome.h>

#include "explorer-debug.h"
#include "explorer-loader.h"


enum {
	STATUS_MESSAGE,
	OPEN_FAILED,
	OPEN_DONE,
	LOAD_FAILED,
	LOAD_DONE,
	LAST_SIGNAL
};

static GtkObjectClass *parent_class = NULL;
static guint signals[LAST_SIGNAL] = { 0 };


/* GtkObject methods.  */

static void
destroy (GtkObject *object)
{
	ExplorerLoader *loader;

	EXPLORER_DEBUG (("Entering function."));

	loader = EXPLORER_LOADER (object);

	explorer_loader_stop (loader);

	gnome_vfs_uri_unref (loader->uri);
	g_free (loader->mime_type);

	if (loader->load_interface_objref != CORBA_OBJECT_NIL) {
		CORBA_Environment ev;

		CORBA_exception_init (&ev);
		CORBA_Object_release (loader->load_interface_objref, &ev);
		CORBA_exception_free (&ev);
	}
}


/* Signals.  */

static void
real_open_failed (ExplorerLoader *loader,
		  GnomeVFSResult result)
{
	g_return_if_fail (loader != NULL);
	g_return_if_fail (EXPLORER_IS_LOADER (loader));
}

static void
real_open_done (ExplorerLoader *loader,
		const gchar *uri)
{
	g_return_if_fail (loader != NULL);
	g_return_if_fail (EXPLORER_IS_LOADER (loader));
}

static void
real_load_failed (ExplorerLoader *loader,
		  GnomeVFSResult result)
{
	g_return_if_fail (loader != NULL);
	g_return_if_fail (EXPLORER_IS_LOADER (loader));
}

static void
real_load_done (ExplorerLoader *loader)
{
	g_return_if_fail (loader != NULL);
	g_return_if_fail (EXPLORER_IS_LOADER (loader));
}


static void
class_init (ExplorerLoaderClass *class)
{
	GtkObjectClass *object_class;

	object_class = (GtkObjectClass*) class;

	object_class->destroy = destroy;

	parent_class = gtk_type_class (gtk_object_get_type ());

	signals[STATUS_MESSAGE] = 
		gtk_signal_new ("status_message",
				GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (ExplorerLoaderClass,
						   status_message),
				gtk_marshal_NONE__STRING,
				GTK_TYPE_NONE, 1,
				GTK_TYPE_STRING);
	
	signals[OPEN_FAILED] = 
		gtk_signal_new ("open_failed",
				GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (ExplorerLoaderClass,
						   open_failed),
				gtk_marshal_NONE__UINT,
				GTK_TYPE_NONE, 1,
				GTK_TYPE_UINT);
	signals[OPEN_DONE] = 
		gtk_signal_new ("open_done",
				GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (ExplorerLoaderClass,
						   open_done),
				gtk_marshal_NONE__POINTER,
				GTK_TYPE_NONE, 1,
				GTK_TYPE_POINTER);
	signals[LOAD_FAILED] = 
		gtk_signal_new ("load_failed",
				GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (ExplorerLoaderClass,
						   load_failed),
				gtk_marshal_NONE__UINT,
				GTK_TYPE_NONE, 1,
				GTK_TYPE_UINT);
	signals[LOAD_DONE] = 
		gtk_signal_new ("load_done",
				GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (ExplorerLoaderClass,
						   load_done),
				gtk_marshal_NONE__NONE,
				GTK_TYPE_NONE, 0);

	gtk_object_class_add_signals (object_class, signals, LAST_SIGNAL);

	class->open_failed = real_open_failed;
	class->open_done = real_open_done;
	class->load_failed = real_load_failed;
	class->load_done = real_load_done;
}

static void
init (ExplorerLoader *loader)
{
	loader->vfs_async_handle = NULL;
	loader->uri = NULL;
	loader->mime_type = NULL;
	loader->channel = NULL;
	loader->load_interface_objref = CORBA_OBJECT_NIL;
}


static void
open_cb (GnomeVFSAsyncHandle *handle,
	 GIOChannel *channel,
	 GnomeVFSResult result,
	 gpointer callback_data)
{
	ExplorerLoader *loader;

	loader = EXPLORER_LOADER (callback_data);

	if (result == GNOME_VFS_OK) {
		g_io_channel_ref (channel);
		loader->channel = channel;
		g_free (loader->mime_type);
		loader->mime_type = g_strdup ("text/html"); /* FIXME */
		gtk_signal_emit (GTK_OBJECT (loader), signals[OPEN_DONE],
				 loader->mime_type);
	} else {
		gtk_signal_emit (GTK_OBJECT (loader),
				 signals[OPEN_FAILED], result);
		return;
	}
}


#define BUFFER_SIZE 16384

static void
feed (ExplorerLoader *loader,
      CORBA_Environment *ev)
{
	gchar buffer[BUFFER_SIZE];
	gint bytes_read;
	GIOError result;

	do {
		EXPLORER_DEBUG(("Reading %u bytes from loader channel", sizeof (buffer)));
		result = g_io_channel_read (loader->channel,
					    buffer, sizeof (buffer),
					    &bytes_read);
	} while (result == G_IO_ERROR_AGAIN);

	if (bytes_read != 0) {
		GNOME_ProgressiveDataSink_iobuf iobuf;

		EXPLORER_DEBUG (("Feeding %d bytes", bytes_read));

		iobuf._length = bytes_read;
		iobuf._maximum = sizeof (iobuf);
		iobuf._buffer = buffer;

		GNOME_ProgressiveDataSink_add_data
			(loader->load_interface_objref, &iobuf, ev);

		/* FIXME check for errors.  */
	}
}

static gboolean
channel_cb (GIOChannel *source,
	    GIOCondition condition,
	    gpointer data)
{
	ExplorerLoader *loader;
	CORBA_Environment ev;
	gboolean retval;

	CORBA_exception_init (&ev);

	loader = EXPLORER_LOADER (data);

	retval = FALSE;

	if  (condition & G_IO_IN) {
		feed (loader, &ev);
		retval = TRUE;
	}

	if (condition & (G_IO_HUP | G_IO_ERR) || ! (condition & G_IO_IN)) {
		/* FIXME: here, we would like to know more about the error.  We
                   need a VFS function to request why the pipe has been shut
                   down.  */
		gtk_signal_emit (GTK_OBJECT (loader), signals[LOAD_DONE]);
		GNOME_ProgressiveDataSink_end (loader->load_interface_objref,
					       &ev);
		explorer_loader_stop (loader);
		retval = FALSE;
		EXPLORER_DEBUG (("Done loading."));
	}

	CORBA_exception_free (&ev);

	return retval;
}


GtkType
explorer_loader_get_type (void)
{
	static GtkType loader_type = 0;

	if (!loader_type) {
		static const GtkTypeInfo loader_info = {
			"ExplorerLoader",
			sizeof (ExplorerLoader),
			sizeof (ExplorerLoaderClass),
			(GtkClassInitFunc) class_init,
			(GtkObjectInitFunc) init,
			/* reserved_1 */ NULL,
			/* reserved_2 */ NULL,
			(GtkClassInitFunc) NULL,
		};

		loader_type = gtk_type_unique (gtk_object_get_type (),
					       &loader_info);
	}

	return loader_type;
}

static void
status_callback (const gchar* message, gpointer user_data)
{
	ExplorerLoader *loader;

	loader = user_data;

	/* We are almost certainly in the VFS slave thread,
	   not in the main thread, so grab the GTK lock */
	
	gdk_threads_enter();
	
	explorer_loader_status_message (loader, message);
	
	gdk_threads_leave();
}

/**
 * explorer_loader_new:
 * @uri: URI object representing an URI that must be loaded
 * 
 * Create a new loader object loading from @uri
 * 
 * Return value: A pointer to the new ExplorerLoader object.
 **/
ExplorerLoader *
explorer_loader_new (const GnomeVFSURI *uri)
{
	GnomeVFSResult result;
	ExplorerLoader *new;

	new = gtk_type_new (explorer_loader_get_type ());
	new->uri = gnome_vfs_uri_dup (uri);

	result = gnome_vfs_async_open_uri_as_channel (&new->vfs_async_handle,
						      new->uri,
						      GNOME_VFS_OPEN_READ,
						      BUFFER_SIZE,
						      open_cb, new);
	if (result != GNOME_VFS_OK) {
		gtk_object_destroy (GTK_OBJECT (new));
		g_warning (_("Cannot start async open: %s"),
			   gnome_vfs_result_to_string (result));
		return NULL;
	} else {
		gnome_vfs_async_add_status_callback (new->vfs_async_handle,
						     status_callback,
						     new);
	}

	return new;
}


static gboolean
set_progressive_data_sink (ExplorerLoader *loader,
			   GNOME_ProgressiveDataSink pdsink,
			   CORBA_Environment *ev)
{
	GNOME_ProgressiveDataSink_start (pdsink, ev);
	if (ev->_major != CORBA_NO_EXCEPTION) {
		EXPLORER_DEBUG (("Cannot start ProgressiveDataSink"));
		return FALSE;
	}

	loader->load_interface_objref = CORBA_Object_duplicate (pdsink, ev);
	if (ev->_major != CORBA_NO_EXCEPTION) {
		EXPLORER_DEBUG (("Cannot duplicate ProgressiveDataSink objref"));
		return FALSE;
	}

	g_io_add_watch_full (loader->channel,
			     G_PRIORITY_LOW,
			     G_IO_IN | G_IO_ERR | G_IO_HUP,
			     channel_cb,
			     loader,
			     NULL);

	return TRUE;
}

/**
 * explorer_loader_set_progressive_data_sink:
 * @loader: A loader object
 * @pdsink: A GNOME_ProgressiveDataSink object reference
 * 
 * Specify @pdsink as the GNOME::ProgressiveDataSink into which data must be
 * fed.
 * 
 * Return value: %TRUE if successful, %FALSE otherwise.
 **/
gboolean
explorer_loader_set_progressive_data_sink (ExplorerLoader *loader,
					   GNOME_ProgressiveDataSink pdsink)
{
	CORBA_Environment ev;
	gboolean retval;

	g_return_val_if_fail (loader != NULL, FALSE);
	g_return_val_if_fail (EXPLORER_IS_LOADER (loader), FALSE);
	g_return_val_if_fail (pdsink != CORBA_OBJECT_NIL, FALSE);

	CORBA_exception_init (&ev);
	retval = set_progressive_data_sink (loader, pdsink, &ev);
	CORBA_exception_free (&ev);

	return retval;
}

/**
 * explorer_loader_set_component:
 * @loader: A loader object
 * @object: A GnomeObject component
 * 
 * Specify @object as the component into which data must be fed.  This function
 * will query for the GNOME::ProgressiveDataSink interface and use it.
 * 
 * Return value: %TRUE if successful, %FALSE otherwise.
 **/
gboolean
explorer_loader_set_component (ExplorerLoader *loader,
			       GnomeObject *object)
{
	CORBA_Environment ev;
	GNOME_ProgressiveDataSink pdsink;
	gboolean retval;

	g_return_val_if_fail (loader != NULL, FALSE);
	g_return_val_if_fail (EXPLORER_IS_LOADER (loader), FALSE);
	g_return_val_if_fail (object != NULL, FALSE);

	CORBA_exception_init (&ev);

	EXPLORER_DEBUG (("Handling plain file: querying ProgressiveDatasink"));

	pdsink = GNOME_Unknown_query_interface
		(gnome_object_corba_objref (object),
		 "IDL:GNOME/ProgressiveDataSink:1.0",
		 &ev);
	if (pdsink == CORBA_OBJECT_NIL) {
		EXPLORER_DEBUG (("No ProgressiveDatasink interface"));
		CORBA_exception_free (&ev);
		return FALSE;
	}

	retval = set_progressive_data_sink (loader, pdsink, &ev);

	CORBA_Object_release (pdsink, &ev);

	CORBA_exception_free (&ev);

	return retval;
}

/**
 * explorer_loader_stop:
 * @loader: A loader object
 * 
 * Stop @loader from loading.
 **/
void
explorer_loader_stop (ExplorerLoader *loader)
{
	g_return_if_fail (loader != NULL);
	g_return_if_fail (EXPLORER_IS_LOADER (loader));

	EXPLORER_DEBUG (("Entering function"));

	if (loader->vfs_async_handle != NULL) {
		EXPLORER_DEBUG (("We do have a handle."));
		gnome_vfs_async_cancel (loader->vfs_async_handle);
		loader->vfs_async_handle = NULL;
		if (loader->channel != NULL) {
			EXPLORER_DEBUG (("Closing channel."));
			g_io_channel_close (loader->channel);
			g_io_channel_unref (loader->channel);
			loader->channel = NULL;
		}
	}
}

/**
 * explorer_loader_get_uri:
 * @loader: A loader object 
 * 
 * Retrieve the URI which @loader is loading.
 * 
 * Return value: A pointer to a GnomeVFSURI representing the URI @loader is
 * loading
 **/
const GnomeVFSURI *
explorer_loader_get_uri (ExplorerLoader *loader)
{
	g_return_val_if_fail (loader != NULL, NULL);
	g_return_val_if_fail (EXPLORER_IS_LOADER (loader), NULL);

	return loader->uri;
}

/**
 * explorer_loader_get_mime_type:
 * @loader: A loader object
 * 
 * Retrieve the MIME type of the file that the loader is loading.
 * 
 * Return value: A pointer to a string representing the MIME type.
 **/
const gchar *
explorer_loader_get_mime_type (ExplorerLoader *loader)
{
	g_return_val_if_fail (loader != NULL, NULL);
	g_return_val_if_fail (EXPLORER_IS_LOADER (loader), NULL);

	return loader->mime_type;
}

/**
 * explorer_loader_status_message:
 * @loader: A loader object 
 * 
 * Emit the "status_message" signal indicating that there's a user-readable
 * message you might put in the status bar.
 * 
 **/
void
explorer_loader_status_message(ExplorerLoader *loader,
			       const gchar *message)
{
	g_return_if_fail (loader != NULL);
	g_return_if_fail (EXPLORER_IS_LOADER (loader));
	g_return_if_fail (message != NULL);

	EXPLORER_DEBUG(("Entering Function"));

	gtk_signal_emit (GTK_OBJECT(loader), signals[STATUS_MESSAGE], message);
}




