/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* explorer-window-menu-handler.h
 *
 * Copyright (C) 1999  Free Software Foundaton
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Author: Ettore Perazzoli
 */

#ifndef _EXPLORER_WINDOW_MENU_BAR_H
#define _EXPLORER_WINDOW_MENU_BAR_H

typedef struct _ExplorerWindowMenuHandler ExplorerWindowMenuHandler;

#include "explorer-window.h"

struct _ExplorerWindowMenuHandler {
	gpointer nothing_for_now;
};


ExplorerWindowMenuHandler *
	explorer_window_menu_handler_new (ExplorerWindow *window);
void	explorer_window_menu_handler_destroy (ExplorerWindowMenuHandler *handler);

#endif /* _EXPLORER_WINDOW_MENUBAR_H */
