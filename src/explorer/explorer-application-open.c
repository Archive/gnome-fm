/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* explorer-application-open.c - Implementation of the Explorer's actions.

   Copyright (C) 1999 Free Software Foundation

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public
   License along with this program; see the file COPYING.  If not,
   write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.

   Author: Ettore Perazzoli <ettore@gnu.org>
*/

/* FIXME probably some parts of these file should be moved into a separate,
   more general-purpose module.  */

#include <gtk/gtk.h>

#include "explorer.h"
#include "error.h"


/* TODO use terminal */

static gboolean
exec_command (const gchar *command)
{
	GnomeVFSProcess *process;
	gchar *argv[4];

	argv[0] = g_strdup ("/bin/sh");
	argv[1] = g_strdup ("-c");
	argv[2] = g_strdup (command);
	argv[3] = NULL;

	process = gnome_vfs_process_new ("/bin/sh", argv,
					 (GNOME_VFS_PROCESS_SETSID
					  | GNOME_VFS_PROCESS_CLOSEFDS),
					 NULL, NULL, NULL, NULL);
	if (process == NULL)
		return FALSE;

	gnome_vfs_process_free (process);
	return TRUE;
}

static gchar *
quote_file_name (const gchar *file_name)
{
	guint len;
	const gchar *p;
	gchar *q;
	gchar *new;

	len = 2;
	for (p = file_name; *p != 0; p++) {
		if (*p == '\'')
			len += 3;
		else
			len++;
	}

	new = g_malloc (len + 1);
	new[0] = '\'';

	for (p = file_name, q = new + 1; *p != 0; p++) {
		if (*p == '\'') {
			q[0] = '"';
			q[1] = '\'';
			q[2] = '"';
			q += 3;
		} else {
			*q = *p;
			q++;
		}
	}

	*q++ = '\'';
	*q = 0;

	return new;
}

static gchar *
expand_command (const gchar *action_string,
		const GnomeVFSURI *uri)
{
	const gchar *p;
	gchar *buf;
	guint len, alloc_len;
	gchar *path, *quoted_path;
	guint quoted_path_len;
	guint i;

	/* FIXME we should _really_ deal with non-local file systems.  */
	if (! gnome_vfs_uri_is_local (uri))
		return NULL;

	/* `GNOME_VFS_URI_HIDE_TOPLEVEL_METHOD' removes the `file:' prefix.  */
	path = gnome_vfs_uri_to_string (uri,
					GNOME_VFS_URI_HIDE_TOPLEVEL_METHOD);
	if (path == NULL)
		return NULL;	/* Eeek!  */

	quoted_path = quote_file_name (path);
	quoted_path_len = strlen (quoted_path);
	g_free (path);

	len = strlen (action_string) + 1;
	alloc_len = len * 2;
	buf = g_malloc (alloc_len);

	i = 0;
	for (p = action_string; *p != '\0'; ) {
		if (*p != '%') {
			buf[i] = *p;
			i++, p++;
		} else {
			switch (p[1]) {
			case '%':
				buf[i] = '%';
				i++, p += 2;
				len--;
				break;
			case 'f':
				while (len + quoted_path_len - 2 >= alloc_len)
					alloc_len *= 2;
				buf = g_realloc (buf, alloc_len);
				memcpy (buf + i, quoted_path, quoted_path_len);
				i += quoted_path_len, p += 2;
				len += quoted_path_len - 2;
				break;
			default:
				buf[i] = p[0];
				buf[i + 1] = p[1];
				i += 2, p += 2;
				break;
			}
		}
	}

	buf[i] = 0;
	g_free (quoted_path);

	return buf;
}

/* FIXME TODO : deal with non-local files.  */
static gboolean
try_external_open (ExplorerApplication *application,
		   const GnomeVFSURI *uri,
		   ExplorerWindow *src_window,
		   const gchar *mime_type,
		   const gchar *action_id,
		   ExplorerApplicationOpenOptions options)
{
	const gchar *action;
	gchar *command;

	/* FIXME temp kludge */
	if (strcmp (mime_type, "text/html") == 0)
		return FALSE;

	action = gnome_mime_get_value (mime_type, "open");
	if (action == NULL)
		return FALSE;

	EXPLORER_DEBUG (("Action command is `%s'.", action));

	command = expand_command (action, uri);

	if (command != NULL) {
		if (! exec_command (command)) {
			/* FIXME be more verbose.  */
			error (GTK_WIDGET (src_window),
			       _("Cannot execute the program associated with\n"
				 "MIME type `%s'."),
			       mime_type);
			/* Return success, so that we don't try something
                           different afterwards. */
			return TRUE;
		}

		g_free (command);
		return TRUE;
	}

	return FALSE;
}


/* This info is used during the asynchronous load process.  */

struct _OpenInfo {
	ExplorerApplication *application;
	ExplorerWindow *src_window;
	GnomeVFSURI *uri;
	gchar *action_id;
	ExplorerApplicationOpenOptions options;
};
typedef struct _OpenInfo OpenInfo;

static OpenInfo *
open_info_new (ExplorerApplication *application,
	       ExplorerWindow *src_window,
	       const GnomeVFSURI *uri,
	       const gchar *action_id,
	       ExplorerApplicationOpenOptions options)
{
	OpenInfo *new;

	new = g_new (OpenInfo, 1);

	new->application = application;
	new->src_window = src_window;
	new->uri = gnome_vfs_uri_dup (uri);
	new->action_id = g_strdup (action_id);
	new->options = options;

	return new;
}

static void
open_info_free (OpenInfo *info)
{
	gnome_vfs_uri_destroy (info->uri);
	g_free (info->action_id);
	g_free (info);
}


/* Loader callbacks.  */

static void
open_failed_cb (ExplorerLoader *loader,
		GnomeVFSResult result,
		gpointer data)
{
	OpenInfo *info;

	EXPLORER_DEBUG (("Entering function"));

	info = (OpenInfo *) data;

	if (result != GNOME_VFS_ERROR_ISDIRECTORY) {
		gchar *uri_string;

		/* FIXME trim */
		uri_string = gnome_vfs_uri_to_string
			(info->uri, GNOME_VFS_URI_HIDE_PASSWORD);
		error (GTK_WIDGET (info->src_window),
		       _("Cannot open `%s':\n%s"), uri_string,
		       gnome_vfs_result_to_string (result));
		g_free (uri_string);
		open_info_free (info);
		return;
	}

	/* It's a directory.  */

	gtk_object_destroy (GTK_OBJECT (loader));

	/* FIXME this is not nice.  */

	explorer_application_open (info->application, info->uri,
				   info->src_window, "special/directory",
				   info->action_id, info->options);

	open_info_free (info);
}

static void
open_done_cb (ExplorerLoader *loader,
	      const gchar *mime_type,
	      gpointer data)
{
	ExplorerWindow *window;
	OpenInfo *info;
	gboolean failed;

	EXPLORER_DEBUG (("Entering function"));

	info = (OpenInfo *) data;

	/* (If we got here, we are sure it is not a directory.  ExplorerLoader
           cannot deal with directories.) */

	if (info->options & EXPLORER_APPLICATION_OPEN_SAMEWINDOW) {
		window = info->src_window;
		failed = ! explorer_window_set_loader (window, loader);
	} else {
		GtkWidget *window_widget;

		window_widget = explorer_application_new_window_with_loader
			(info->application, loader);

		if (window_widget == NULL) {
			window = NULL;
			failed = TRUE;
		} else {
			window = EXPLORER_WINDOW (window_widget);
			failed = FALSE;
		}
	}

	if (failed) {
		gtk_object_destroy (GTK_OBJECT (loader));
		if (! try_external_open (info->application,
					 info->uri,
					 info->src_window,
					 mime_type,
					 info->action_id,
					 info->options)) {
			/* FIXME open as */
		}
	}

	open_info_free (info);
}


static void
open_directory (ExplorerApplication *application,
		const GnomeVFSURI *uri,
		ExplorerWindow *src_window,
		ExplorerApplicationOpenOptions options)
{
	GtkWidget *window;

	if (options & EXPLORER_APPLICATION_OPEN_SAMEWINDOW)
		window = GTK_WIDGET (src_window);
	else
		window = explorer_application_new_window (application);

	explorer_window_load_directory (EXPLORER_WINDOW (window), uri);
}

static void
open_unknown (ExplorerApplication *application,
	      const GnomeVFSURI *uri,
	      ExplorerWindow *src_window,
	      const gchar *action_id,
	      ExplorerApplicationOpenOptions options)
{
	ExplorerLoader *loader;
	OpenInfo *info;

	/* Let's open the file so that we check out the MIME type.  We get here
           if we don't know the MIME type for the file, or we know that the
           file is not a directory.  */

	/* Actually, we could only use `gnome_vfs_get_file_info()', but
	   `get_file_info()' results in an `open()' for slow file
	   systems, so in practice this is more convenient as it saves
	   us from a second `open()' when the file can be loaded into
	   an embedded view or must be copied into a temporary file.  */

	loader = explorer_loader_new (uri);
	if (loader == NULL) {
		error (GTK_WIDGET (src_window),
		       _("Specified URI cannot be opened because of an internal error."));
		return;
	}

	info = open_info_new (application, src_window, uri, action_id,
			      options);

	gtk_signal_connect (GTK_OBJECT (loader), "open_failed",
			    GTK_SIGNAL_FUNC (open_failed_cb), info);
	gtk_signal_connect (GTK_OBJECT (loader), "open_done",
			    GTK_SIGNAL_FUNC (open_done_cb), info);
}


/**
 * explorer_application_open:
 * @application: An ExplorerApplication.
 * @uri: URI to be opened.
 * @src_window: Window that triggers this action.
 * @mime_type: The MIME type of the URI that we are going to open.  If the type
 * is not known yet, this parameter should be set to be NULL.
 * @action_id: Sub ID string of the open action; NULL if no sub ID is needed.
 * @options: Options for the action.
 * 
 * This function implements the "open" action in Explorer.
 **/
void
explorer_application_open (ExplorerApplication *application,
			   const GnomeVFSURI *uri,
			   ExplorerWindow *src_window,
			   const gchar *mime_type,
			   const gchar *action_id,
			   ExplorerApplicationOpenOptions options)
{
	GnomeVFSURI *uri_with_slash;

	if (mime_type != NULL && strcmp (mime_type, "special/directory") != 0) {
		if (try_external_open (application, uri, src_window, mime_type,
				       action_id, options))
			return;
	}

	/* UGLY KLUDGE ALERT.  This *sucks*.  */
	uri_with_slash = gnome_vfs_uri_append_path (uri, "");

	/* First check if this URI is already open somewhere, unless we are
           forced to open the directory in the same window or in a new one.  */

	if (! (options & (EXPLORER_APPLICATION_OPEN_SAMEWINDOW
			  | EXPLORER_APPLICATION_OPEN_NEWWINDOW))) {
		GList *window_list, *p;

		window_list = explorer_application_get_window_list
								  (application);

		for (p = window_list; p != NULL; p = p->next) {
			ExplorerWindow *window;
			const GnomeVFSURI *window_uri;

			window = p->data;
			window_uri = explorer_window_get_current_uri (window);
			if (window_uri == NULL)
				continue;

			if (gnome_vfs_uri_equal (uri, window_uri)
			    || gnome_vfs_uri_equal (uri_with_slash,
						    window_uri)) {
				gdk_window_show (GTK_WIDGET (window)->window);
				gnome_vfs_uri_unref (uri_with_slash);
				return;
			}
		}
	}

	if (mime_type != NULL && strcmp (mime_type, "special/directory") == 0)
		open_directory (application, uri, src_window, options);
	else
		open_unknown (application, uri, src_window, action_id, options);

	gnome_vfs_uri_unref (uri_with_slash);
}
