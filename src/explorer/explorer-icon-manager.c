/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* explorer-icon-manager.c

   Copyright (C) 1999 Free Software Foundation

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public
   License along with this program; see the file COPYING.  If not,
   write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.

   Authors: Ettore Perazzoli <ettore@gnu.org>
            Miguel de Icaza <miguel@gnu.org>
            Federico Mena Quintero <federico@redhat.com>
*/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gnome.h>

#include "explorer-icon-manager.h"


struct _IconSet {
	gchar *name;
	GdkImlibImage *plain;
	GdkImlibImage *symlink;
};
typedef struct _IconSet IconSet;

struct _ExplorerIconManager {
	ExplorerApplication *application;

	GHashTable *name_to_image;

	IconSet *iset_directory;
	IconSet *iset_dirclosed;
	IconSet *iset_executable;
	IconSet *iset_regular;
	IconSet *iset_core;
	IconSet *iset_socket;
	IconSet *iset_fifo;
	IconSet *iset_chardevice;
	IconSet *iset_blockdevice;
	
	GdkImlibImage *symlink_overlay;
};


static IconSet *
icon_set_new (const gchar *name)
{
	IconSet *new;

	new = g_new (IconSet, 1);
	new->name = g_strdup (name);
	new->plain = NULL;
	new->symlink = NULL;

	return new;
}

static void
icon_set_destroy (IconSet *icon_set)
{
	if (icon_set != NULL) {
		g_free (icon_set->name);
		gdk_imlib_destroy_image (icon_set->plain);
		gdk_imlib_destroy_image (icon_set->symlink);
	}
}


/* Builds a composite of the plain image and the litle symlink icon */
static GdkImlibImage *
build_overlay (GdkImlibImage *plain, GdkImlibImage *overlay)
{
	int rowstride;
	int overlay_rowstride;
	guchar *src, *dest;
	int y;
	GdkImlibImage *im;

	im = gdk_imlib_clone_image (plain);

	rowstride = plain->rgb_width * 3;
	overlay_rowstride = overlay->rgb_width * 3;

	dest = im->rgb_data + ((plain->rgb_height - overlay->rgb_height) * rowstride
			       + (plain->rgb_width - overlay->rgb_width) * 3);

	src = overlay->rgb_data;

	for (y = 0; y < overlay->rgb_height; y++) {
		memcpy (dest, src, overlay_rowstride);

		dest += rowstride;
		src += overlay_rowstride;
	}

	gdk_imlib_changed_image (im);
	return im;
}


static GdkImlibImage *
get_stock_image (ExplorerIconManager *manager,
		 const gchar *name)
{
	GdkImlibImage *image;
	gchar *full_name;
	const gchar *icon_directory;

	icon_directory = explorer_application_get_icon_path (manager->application);

	full_name = g_concat_dir_and_file (icon_directory, name);
	image = gdk_imlib_load_image (full_name);
	g_free (full_name);

	return image;
}

static IconSet *
get_stock_icon_set (ExplorerIconManager *manager,
		    const gchar *name)
{
	GdkImlibImage *image;
	IconSet *new;
	gchar *full_name;
	const gchar *icon_directory;

	icon_directory = explorer_application_get_icon_path (manager->application);

	full_name = g_concat_dir_and_file (icon_directory, name);
	image = gdk_imlib_load_image (full_name);
	g_free (full_name);

	new = icon_set_new (name);
	new->plain = image;

	g_hash_table_insert (manager->name_to_image, new->name, new);

	return new;
}

static IconSet *
get_icon_set_for_info (ExplorerIconManager *manager,
		       const GnomeVFSFileInfo *info)
{
	if (info->permissions & (GNOME_VFS_PERM_USER_EXEC
				 | GNOME_VFS_PERM_GROUP_EXEC
				 | GNOME_VFS_PERM_OTHER_EXEC))
		return manager->iset_executable;

	switch (info->type) {
	case GNOME_VFS_FILE_TYPE_REGULAR:
		return manager->iset_regular;
	case GNOME_VFS_FILE_TYPE_DIRECTORY:
		return manager->iset_directory;
	case GNOME_VFS_FILE_TYPE_FIFO:
		return manager->iset_fifo;
	case GNOME_VFS_FILE_TYPE_SOCKET:
		return manager->iset_socket;
	case GNOME_VFS_FILE_TYPE_CHARDEVICE:
		return manager->iset_chardevice;
	case GNOME_VFS_FILE_TYPE_BLOCKDEVICE:
		return manager->iset_blockdevice;
	case GNOME_VFS_FILE_TYPE_BROKENSYMLINK:
	case GNOME_VFS_FILE_TYPE_UNKNOWN:
	default:
		return manager->iset_regular; /* FIXME */
	}
}

static IconSet *
get_icon_set_for_name (ExplorerIconManager *manager,
		       const gchar *name)
{
	GdkImlibImage *plain;
	IconSet *set;
	gchar *full_name;
	const gchar *icon_directory;

	set = g_hash_table_lookup (manager->name_to_image, name);
	if (set != NULL)
		return set;

	/* Absolute path.  */
	if (name[0] == '/' || name[0] == '~') {
		/* Evil cast, Imlib sucks.  */
		plain = gdk_imlib_load_image ((gchar *) name);
		if (plain == NULL)
			return NULL;
	} else {
		icon_directory = explorer_application_get_icon_path (manager->application);
		full_name = g_concat_dir_and_file (icon_directory, name);
		plain = gdk_imlib_load_image (full_name);
		if (plain == NULL)
			return NULL;
		g_free (full_name);
	}

	set = icon_set_new (name);
	set->plain = plain;

	g_hash_table_insert (manager->name_to_image, set->name, set);

	return set;
}


ExplorerIconManager *
explorer_icon_manager_new (ExplorerApplication *application)
{
	ExplorerIconManager *new;

	g_return_val_if_fail (application != NULL, NULL);

	new = g_new0 (ExplorerIconManager, 1);

	new->application = application;

	new->name_to_image = g_hash_table_new (g_str_hash, g_str_equal);

	/* Load the default icons.  */

	new->iset_directory   = get_stock_icon_set (new, "i-directory.png");
	new->iset_dirclosed   = get_stock_icon_set (new, "i-dirclosed.png");
	new->iset_executable  = get_stock_icon_set (new, "i-executable.png");
	new->iset_regular     = get_stock_icon_set (new, "i-regular.png");
	new->iset_core        = get_stock_icon_set (new, "i-core.png");
	new->iset_socket      = get_stock_icon_set (new, "i-sock.png");
	new->iset_fifo        = get_stock_icon_set (new, "i-fifo.png");
	new->iset_chardevice  = get_stock_icon_set (new, "i-chardev.png");
	new->iset_blockdevice = get_stock_icon_set (new, "i-blockdev.png");

	/* The symlink overlay.  */

	new->symlink_overlay  = get_stock_image (new, "i-symlink.png");

	if (new->symlink_overlay == NULL
	    || new->iset_directory == NULL
	    || new->iset_dirclosed == NULL
	    || new->iset_executable == NULL
	    || new->iset_regular == NULL
	    || new->iset_core == NULL
	    || new->iset_socket == NULL
	    || new->iset_fifo == NULL
	    || new->iset_chardevice == NULL
	    || new->iset_blockdevice == NULL
	    || new->symlink_overlay == NULL) {
		g_warning (_("One of the default icons were not found.\n"
			     "Please check that the application was installed correctly."));
		explorer_icon_manager_destroy (new);
		return NULL;
	}

	return new;
}

void
explorer_icon_manager_destroy (ExplorerIconManager *manager)
{
	g_return_if_fail (manager != NULL);

	g_hash_table_destroy (manager->name_to_image);

	icon_set_destroy (manager->iset_directory);
	icon_set_destroy (manager->iset_dirclosed);
	icon_set_destroy (manager->iset_executable);
	icon_set_destroy (manager->iset_regular);
	icon_set_destroy (manager->iset_core);
	icon_set_destroy (manager->iset_socket);
	icon_set_destroy (manager->iset_fifo);
	icon_set_destroy (manager->iset_chardevice);
	icon_set_destroy (manager->iset_blockdevice);

	gdk_imlib_destroy_image (manager->symlink_overlay);

	g_free (manager);
}

GdkImlibImage *
explorer_icon_manager_get_icon_for_info (ExplorerIconManager *manager,
					 const GnomeVFSFileInfo *info)
{
	IconSet *icon_set;

	g_return_val_if_fail (manager != NULL, NULL);
	g_return_val_if_fail (info != NULL, NULL);

	if (info->type == GNOME_VFS_FILE_TYPE_DIRECTORY) {
		icon_set = manager->iset_directory;
	} else {
		const gchar *icon_name;

		icon_name = NULL;

		if (info->mime_type != NULL)
			icon_name = gnome_mime_get_value (info->mime_type,
							  "icon-filename");
		if (icon_name == NULL) {
			icon_set = get_icon_set_for_info (manager, info);
		} else {
			icon_set = get_icon_set_for_name (manager, icon_name);
			if (icon_set == NULL) {
				g_warning ("Cannot load image `%s'\n",
					   icon_name);
				/* FIXME should be an icon representing an
                                   unknown type.  */
				icon_set = manager->iset_regular;
			}
		}
	}

	if (GNOME_VFS_FILE_INFO_SYMLINK (info)) {
		if (icon_set->symlink == NULL)
			icon_set->symlink = build_overlay
				(icon_set->plain, manager->symlink_overlay);

		return icon_set->symlink;
	} else {
		return icon_set->plain;
	}
}
