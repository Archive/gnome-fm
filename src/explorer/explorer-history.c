/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* explorer-history.c - History handling for the GNOME Explorer.
 *
 * Copyright (C) 1999 Free Software Foundation
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program; see the file COPYING.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 * 
 * Author: Ettore Perazzoli  */

#include <glib.h>

#include "explorer-history.h"

struct _ExplorerHistory {
	GList *items;
	GList *current_item;
};


/* ExplorerHistoryItem implementation.  */

static ExplorerHistoryItem *
explorer_history_item_new (const GnomeVFSURI *uri)
{
	ExplorerHistoryItem *new;

	new = g_new (ExplorerHistoryItem, 1);

	new->uri = gnome_vfs_uri_dup (uri);
	time (&new->last_visited_time);

	return new;
}

static void
explorer_history_item_destroy (ExplorerHistoryItem *item)
{
	g_return_if_fail (item != NULL);

	gnome_vfs_uri_unref (item->uri);
	g_free (item);
}


/* Utility functions.  */

static void
destroy_history_item_list (GList *list)
{
	GList *p;

	if (list == NULL)
		return;

	for (p = list; p != NULL; p = p->next)
		explorer_history_item_destroy (p->data);

	g_list_free (list);
}


ExplorerHistory	*
explorer_history_new (void)
{
	ExplorerHistory *new;

	new = g_new (ExplorerHistory, 1);

	new->items = NULL;
	new->current_item = NULL;

	return new;
}

void
explorer_history_destroy (ExplorerHistory *history)
{
	g_return_if_fail (history != NULL);

	destroy_history_item_list (history->items);

	g_free (history);
}

gboolean
explorer_history_back (ExplorerHistory *history)
{
	g_return_val_if_fail (history != NULL, FALSE);

	if (history->current_item == NULL
	    || history->current_item->prev == NULL)
		return FALSE;

	history->current_item = history->current_item->prev;
	return TRUE;
}

gboolean
explorer_history_can_back (ExplorerHistory *history)
{
	g_return_val_if_fail (history != NULL, FALSE);

	return (history->current_item != NULL
		&& history->current_item->prev != NULL);
}

gboolean
explorer_history_forward (ExplorerHistory *history)
{
	g_return_val_if_fail (history != NULL, FALSE);

	if (history->current_item == NULL
	    || history->current_item->next == NULL)
		return FALSE;

	history->current_item = history->current_item->next;
	return TRUE;
}

gboolean explorer_history_can_forward (ExplorerHistory *history)
{
	g_return_val_if_fail (history != NULL, FALSE);

	return (history->current_item != NULL
		&& history->current_item->next != NULL);
}

const ExplorerHistoryItem *
explorer_history_get_current (ExplorerHistory *history)
{
	g_return_val_if_fail (history != NULL, NULL);

	return (ExplorerHistoryItem *) history->current_item->data;
}


void
explorer_history_add (ExplorerHistory *history,
		      const GnomeVFSURI *uri)
{
	ExplorerHistoryItem *new_item;
	GList *current_item;

	new_item = explorer_history_item_new (uri);

	current_item = history->current_item;
	if (current_item != NULL && current_item->next != NULL) {
		destroy_history_item_list (current_item->next);
		current_item->next = NULL;
	}

	current_item = g_list_append (current_item, new_item);
	if (history->items == NULL)
		history->current_item = history->items = current_item;
	else
		history->current_item = current_item->next;
}
