/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* explorer-window-toolbar.h - Toolbar for explorer windows.

   Copyright (C) 1999 Free Software Foundation

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public
   License along with this program; see the file COPYING.  If not,
   write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.

   Author: Ettore Perazzoli <ettore@gnu.org>
*/

#ifndef _EXPLORER_WINDOW_TOOLBAR_H
#define _EXPLORER_WINDOW_TOOLBAR_H

#include <gnome.h>

typedef struct _ExplorerWindowToolbar ExplorerWindowToolbar;

#include "explorer-window.h"

#define EXPLORER_WINDOW_TOOLBAR(obj) \
  GTK_CHECK_CAST (obj, explorer_window_toolbar_get_type (), ExplorerWindowToolbar)
#define EXPLORER_WINDOW_TOOLBAR_CLASS(klass) \
  GTK_CHECK_CLASS_CAST (klass, explorer_window_toolbar_get_type (), ExplorerWindowToolbarClass)
#define EXPLORER_IS_TOOLBAR(obj) \
  GTK_CHECK_TYPE (obj, explorer_window_toolbar_get_type ())

struct _ExplorerWindowToolbar {
	GtkToolbar toolbar;

	ExplorerWindow *window;

	gchar *uri;

	GtkWidget *back_button;
	GtkWidget *forward_button;
	GtkWidget *up_button;
	GtkWidget *stop_button;

	GtkWidget *reload_button;
	GtkWidget *home_button;
};

struct _ExplorerWindowToolbarClass {
	GtkToolbarClass parent_class;
};
typedef struct _ExplorerWindowToolbarClass ExplorerWindowToolbarClass;


GtkType		 explorer_window_toolbar_get_type	(void);
ExplorerWindowToolbar  *				
		 explorer_window_toolbar_new		(ExplorerWindow *window);
void		 explorer_window_toolbar_allow_up	(ExplorerWindowToolbar *toolbar,
							 gboolean allow);
void		 explorer_window_toolbar_allow_back	(ExplorerWindowToolbar *toolbar,
							 gboolean allow);
void		 explorer_window_toolbar_allow_forward	(ExplorerWindowToolbar *toolbar,
							 gboolean allow);
void		 explorer_window_toolbar_allow_reload 	(ExplorerWindowToolbar *toolbar,
							 gboolean allow);
void		 explorer_window_toolbar_allow_stop	(ExplorerWindowToolbar *toolbar,
							 gboolean allow);

#endif /* _EXPLORER_WINDOW_TOOLBAR_H */
