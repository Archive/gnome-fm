/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* explorer-history.h - History handling for the GNOME Explorer.
 *
 * Copyright (C) 1999 Free Software Foundation
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program; see the file COPYING.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 * 
 * Author: Ettore Perazzoli  */

#ifndef _EXPLORER_HISTORY_H
#define _EXPLORER_HISTORY_H

#include <libgnomevfs/gnome-vfs.h>


typedef struct _ExplorerHistory ExplorerHistory;

struct _ExplorerHistoryItem {
	GnomeVFSURI *uri;
	time_t last_visited_time;
};
typedef struct _ExplorerHistoryItem ExplorerHistoryItem;


ExplorerHistory	*explorer_history_new 		(void);
void		 explorer_history_destroy	(ExplorerHistory *history);

gboolean	 explorer_history_back  	(ExplorerHistory *history);
gboolean	 explorer_history_can_back  	(ExplorerHistory *history);
gboolean	 explorer_history_forward	(ExplorerHistory *history);
gboolean	 explorer_history_can_forward	(ExplorerHistory *history);

const ExplorerHistoryItem *
		 explorer_history_get_current 	(ExplorerHistory *history);

void		 explorer_history_add		(ExplorerHistory *history,
						 const GnomeVFSURI *uri);

#endif /* _EXPLORER_HISTORY_H */
