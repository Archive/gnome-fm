/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* explorer-application.c - The main explorer application object.

   Copyright (C) 1999 Free Software Foundation

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public
   License along with this program; see the file COPYING.  If not,
   write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.

   Author: Ettore Perazzoli <ettore@gnu.org>
*/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "explorer-debug.h"

#include "explorer-application.h"


struct _ExplorerApplication {
	ExplorerViewManager *view_manager;
	ExplorerIconManager *icon_manager;

	/* List of the currently open windows.  */
	GList *window_list;
};


static void
window_destroy_cb (ExplorerWindow *window,
		   gpointer data)
{
	ExplorerApplication *application;

	application = EXPLORER_APPLICATION (data);
	application->window_list = g_list_remove (application->window_list,
						  window);
	if (g_list_length (application->window_list) == 0)
		gtk_main_quit();
		
}


/* Utility functions.  */

static void
add_new_window (ExplorerApplication *application,
		ExplorerWindow *window)
{
	application->window_list = g_list_prepend (application->window_list,
						   window);

	gtk_signal_connect (GTK_OBJECT (window), "destroy",
			    GTK_SIGNAL_FUNC (window_destroy_cb), application);
}


/**
 * explorer_application_new:
 * 
 * Create a new ExplorerApplication instance.  This is the object that
 * implements all of the Explorer's functionality.
 * 
 * Return value: A pointer to the new ExplorerApplication object.
 **/
ExplorerApplication *
explorer_application_new (void)
{
	ExplorerApplication *new;
	ExplorerViewManager *view_manager;
	ExplorerIconManager *icon_manager;

	view_manager = explorer_view_manager_new ();
	if (view_manager == NULL)
		return NULL;

	new = g_new (ExplorerApplication, 1);

	icon_manager = explorer_icon_manager_new (new);
	if (icon_manager == NULL) {
		g_free (new);
		return NULL;
	}

	new->view_manager = view_manager;
	new->icon_manager = icon_manager;
	new->window_list = NULL;

	return new;
}

/**
 * explorer_application_new_window:
 * @application: An ExplorerApplication.
 * 
 * Create a new window in @application.  You should always use this function to
 * create windows in an Explorer application: this will add the window to the
 * window list and will also install a signal handler to handle its destruction
 * properly.
 * 
 * Return value: A pointer to the new widget.
 **/
GtkWidget *
explorer_application_new_window (ExplorerApplication *application)
{
	GtkWidget *new_window;

	g_return_val_if_fail (application != NULL, NULL);

	new_window = explorer_window_new (application);
	if (new_window == NULL)
		return NULL;

	add_new_window (application, EXPLORER_WINDOW (new_window));

	return new_window;
}

/**
 * explorer_application_new_window_with_loader:
 * @application: An ExplorerApplication.
 * @loader: An ExplorerLoader which has passed the `open' phase successfully
 * (i.e. it has already emitted the "open_done" signal).
 * 
 * Create a new window in @application loading from @loader.  You should always
 * use this function to create windows with a loader in an Explorer
 * application: this will add the window to the window list and will also
 * install a signal handler to handle its destruction properly.
 *
 * For further info, see %explorer_window_new_with_loader.
 * 
 * Return value: A pointer to the new widget.
 **/
GtkWidget *
explorer_application_new_window_with_loader (ExplorerApplication *application,
					     ExplorerLoader *loader)
{
	GtkWidget *new_window;

	g_return_val_if_fail (application != NULL, NULL);
	g_return_val_if_fail (loader != NULL, NULL);
	g_return_val_if_fail (EXPLORER_IS_LOADER (loader), NULL);

	new_window = explorer_window_new_with_loader (application, loader);
	if (new_window == NULL)
		return NULL;

	add_new_window (application, EXPLORER_WINDOW (new_window));

	return new_window;
}


/**
 * explorer_application_get_view_manager:
 * @application: An ExplorerApplication.
 * 
 * Retrieve the view manager for @application.
 * 
 * Return value: A pointer to the view manager object.
 **/
ExplorerViewManager *
explorer_application_get_view_manager (ExplorerApplication *application)
{
	g_return_val_if_fail (application != NULL, NULL);

	return application->view_manager;
}

/**
 * explorer_application_get_icon_manager:
 * @application: An ExplorerApplication.
 * 
 * Retrieve the icon manager for @application.
 * 
 * Return value: A pointer to the icon manager object.
 **/
ExplorerIconManager *
explorer_application_get_icon_manager (ExplorerApplication *application)
{
	g_return_val_if_fail (application != NULL, NULL);

	return application->icon_manager;
}


const gchar *
explorer_application_get_path_prefix (ExplorerApplication *application)
{
	g_return_val_if_fail (application != NULL, NULL);

	return PREFIX;
}

const gchar *
explorer_application_get_icon_path (ExplorerApplication *application)
{
	g_return_val_if_fail (application != NULL, NULL);

	return ICON_PATH;
}

GList *
explorer_application_get_window_list (ExplorerApplication *application)
{
	g_return_val_if_fail (application != NULL, NULL);

	return application->window_list;
}
