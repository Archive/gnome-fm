/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* explorer-dnd.h - Some useful defines for Drag and Drop.

   Copyright (C) 1999 Free Software Foundation

   The Gnome Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   The Gnome Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with the Gnome Library; see the file COPYING.LIB.  If not,
   write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.

   Author: Michael Meeks   <mmeeks@gnu.org>
           Miguel de Icaza <miguel@gnu.org>
*/

#ifndef _EXPLORER_DND_H
#define _EXPLORER_DND_H

/* Standard DnD types */
typedef enum {
	EXPLORER_DND_MC_DESKTOP_ICON,
	EXPLORER_DND_URI_LIST,
	EXPLORER_DND_TEXT_PLAIN,
	EXPLORER_DND_URL,
	EXPLORER_DND_NTARGETS
} ExplorerDndTargetType;

/* DnD target names */
#define EXPLORER_DND_MC_DESKTOP_ICON_TYPE "application/x-mc-desktop-icon"
#define EXPLORER_DND_URI_LIST_TYPE 	  "text/uri-list"
#define EXPLORER_DND_TEXT_PLAIN_TYPE 	  "text/plain"
#define EXPLORER_DND_URL_TYPE		  "_NETSCAPE_URL"


#endif /* _EXPLORER_DND_H */
