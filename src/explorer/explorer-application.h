/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* explorer-application.h - The main explorer application object.

   Copyright (C) 1999 Free Software Foundation

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public
   License along with this program; see the file COPYING.  If not,
   write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.

   Author: Ettore Perazzoli <ettore@gnu.org>
*/

#ifndef _EXPLORER_APPLICATION_H
#define _EXPLORER_APPLICATION_H

#include <glib.h>

typedef struct _ExplorerApplication ExplorerApplication;

#include "explorer-loader.h"
#include "explorer-window.h"
#include "explorer-icon-manager.h"
#include "explorer-view-manager.h"


#define EXPLORER_APPLICATION(a)	((ExplorerApplication *) a)


ExplorerApplication *explorer_application_new
					(void);
GtkWidget	    *explorer_application_new_window
					(ExplorerApplication *application);
GtkWidget 	    *explorer_application_new_window_with_loader
					(ExplorerApplication *application,
					 ExplorerLoader *loader);

ExplorerViewManager *explorer_application_get_view_manager
					(ExplorerApplication *application);
ExplorerIconManager *explorer_application_get_icon_manager
					(ExplorerApplication *application);

const gchar 	    *explorer_application_get_path_prefix
					(ExplorerApplication *application);
const gchar 	    *explorer_application_get_icon_path
					(ExplorerApplication *application);

GList 		    *explorer_application_get_window_list
					(ExplorerApplication *application);

#endif /* _EXPLORER_APPLICATION_H */
