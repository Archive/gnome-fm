/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* explorer-window.h - Explorer window for the GNOME Explorer.

   Copyright (C) 1999 Free Software Foundation

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public
   License along with this program; see the file COPYING.  If not,
   write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.

   Author: Ettore Perazzoli <ettore@gnu.org>
*/

#ifndef _EXPLORER_WINDOW_H
#define _EXPLORER_WINDOW_H

#include <gnome.h>

typedef struct _ExplorerWindow ExplorerWindow;

#include "explorer-application.h"
#include "explorer-directory-view.h"
#include "explorer-history.h"
#include "explorer-loader.h"
#include "explorer-window-layout.h"
#include "explorer-window-menu-handler.h"
#include "explorer-window-toolbar.h"

#define EXPLORER_WINDOW(obj) \
	GTK_CHECK_CAST (obj, explorer_window_get_type (), ExplorerWindow)
#define EXPLORER_WINDOW_CLASS(klass) \
	GTK_CHECK_CLASS_CAST (klass, explorer_window_get_type (), ExplorerWindowClass)
#define EXPLORER_IS_WINDOW(obj) \
	GTK_CHECK_TYPE (obj, explorer_window_get_type ())

enum _ExplorerWindowStatus {
	EXPLORER_WINDOW_STATUS_IDLE,
	EXPLORER_WINDOW_STATUS_OPENING,
	EXPLORER_WINDOW_STATUS_LOADING
};
typedef enum _ExplorerWindowStatus ExplorerWindowStatus;

struct _ExplorerWindow {
	GnomeApp gnome_app;

	ExplorerWindowStatus status;

	ExplorerApplication *application;

	ExplorerWindowMenuHandler *menu_handler;
	ExplorerWindowToolbar *toolbar;

	GtkWidget *frame;
	GtkWidget *location_bar;
	GtkWidget *app_bar;

	ExplorerLoader *loader;
	ExplorerHistory *history;

	ExplorerDirectoryView *directory_view;
	ExplorerDirectoryViewMode directory_view_mode;

	/* URI currently displayed.  */
	GnomeVFSURI *uri;

	/* New URI being loaded.  This is used to avoid destroying the current
           view until a new one has been fully loaded.  */
	GnomeVFSURI *new_uri;

	/* The layout being used to fill this directory view.  */
	ExplorerWindowLayout *layout;

	/* Whether the new URI should be added to the history when loaded.  */
	gboolean add_new_uri_to_history : 1;
};

struct _ExplorerWindowClass {
	GnomeAppClass parent_class;
};
typedef struct _ExplorerWindowClass ExplorerWindowClass;


GtkType		   explorer_window_get_type 	   (void);
GtkWidget	  *explorer_window_new		   (ExplorerApplication *app);
gboolean	   explorer_window_set_loader      (ExplorerWindow *window,
						    ExplorerLoader *loader);
GtkWidget 	  *explorer_window_new_with_loader (ExplorerApplication
						            *application,
						    ExplorerLoader *loader);

gboolean	   explorer_window_load_directory  (ExplorerWindow *window,
						    const GnomeVFSURI *uri);

const GnomeVFSURI *explorer_window_get_current_uri (ExplorerWindow *window);

gboolean	   explorer_window_back		   (ExplorerWindow *window);
gboolean           explorer_window_forward         (ExplorerWindow *window);
gboolean           explorer_window_up_to_parent    (ExplorerWindow *window);
gboolean           explorer_window_home            (ExplorerWindow *window);
gboolean           explorer_window_reload          (ExplorerWindow *window);
gboolean           explorer_window_stop		   (ExplorerWindow *window);

void		   explorer_window_set_directory_view_mode
						   (ExplorerWindow *window,
						    ExplorerDirectoryViewMode
						            mode);

ExplorerWindowLayout *explorer_window_get_layout   (ExplorerWindow *window);

void		   explorer_window_line_up_icons   (ExplorerWindow *window);
void		   explorer_window_sort		   (ExplorerWindow *window,
						    ExplorerDirectoryViewSortType sort_type);

#endif /* _EXPLORER_WINDOW_H */
