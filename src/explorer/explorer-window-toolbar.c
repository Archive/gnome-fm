/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* explorer-window-toolbar.c - Toolbar for explorer windows.

   Copyright (C) 1999 Free Software Foundation

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public
   License along with this program; see the file COPYING.  If not,
   write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.

   Author: Ettore Perazzoli <ettore@gnu.org>
*/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gnome.h>

#include "explorer-window-toolbar.h"


static GtkToolbarClass *parent_class;


static void
back_cb (GtkWidget *button,
	 gpointer data)
{
	ExplorerWindowToolbar *toolbar;

	toolbar = EXPLORER_WINDOW_TOOLBAR (data);
	explorer_window_back (toolbar->window);
}

static void
forward_cb (GtkWidget *button,
	    gpointer data)
{
	ExplorerWindowToolbar *toolbar;

	toolbar = EXPLORER_WINDOW_TOOLBAR (data);
	explorer_window_forward (toolbar->window);
}

static void
up_cb (GtkWidget *button,
       gpointer data)
{
	ExplorerWindowToolbar *toolbar;

	toolbar = EXPLORER_WINDOW_TOOLBAR (data);
	explorer_window_up_to_parent (toolbar->window);
}

static void
reload_cb (GtkWidget *button,
	   gpointer data)
{
	ExplorerWindowToolbar *toolbar;

	toolbar = EXPLORER_WINDOW_TOOLBAR (data);
	explorer_window_reload (toolbar->window);
}

static void
home_cb (GtkWidget *button,
	 gpointer data)
{
	ExplorerWindowToolbar *toolbar;

	toolbar = EXPLORER_WINDOW_TOOLBAR (data);
	explorer_window_home (toolbar->window);
}

static void
stop_cb (GtkWidget *button,
	 gpointer data)
{
	ExplorerWindowToolbar *toolbar;

	toolbar = EXPLORER_WINDOW_TOOLBAR (data);
	explorer_window_stop (toolbar->window);
}


static void
class_init (ExplorerWindowToolbarClass *class)
{
	GtkObjectClass *object_class;

	object_class = GTK_OBJECT_CLASS (class);

	parent_class = gtk_type_class (gtk_toolbar_get_type ());
}

static void
init (ExplorerWindowToolbar *toolbar)
{
	GnomeUIInfo ui_info[] = {
		GNOMEUIINFO_ITEM_STOCK
			(N_("Back"), N_("Go to the previously visited directory"),
			 back_cb, GNOME_STOCK_PIXMAP_BACK),
		GNOMEUIINFO_ITEM_STOCK
			(N_("Forward"), N_("Go to the next directory"),
			 forward_cb, GNOME_STOCK_PIXMAP_FORWARD),
		GNOMEUIINFO_ITEM_STOCK
			(N_("Up"), N_("Go up a level in the directory heirarchy"),
			 up_cb, GNOME_STOCK_PIXMAP_UP),
		GNOMEUIINFO_ITEM_STOCK
			(N_("Reload"), N_("Reload this view"),
			 reload_cb, GNOME_STOCK_PIXMAP_REFRESH),
		GNOMEUIINFO_SEPARATOR,
		GNOMEUIINFO_ITEM_STOCK
			(N_("Home"), N_("Go to your home directory"),
			 home_cb, GNOME_STOCK_PIXMAP_HOME),
		GNOMEUIINFO_SEPARATOR,
		GNOMEUIINFO_ITEM_STOCK
			(N_("Stop"), N_("Interrupt loading"),
			 stop_cb, GNOME_STOCK_PIXMAP_STOP),
		GNOMEUIINFO_END
	};

	gtk_toolbar_set_style (GTK_TOOLBAR (toolbar), GTK_TOOLBAR_BOTH);
	gnome_app_fill_toolbar_with_data (GTK_TOOLBAR (toolbar), ui_info,
					  NULL, toolbar);

	toolbar->window = NULL;

	toolbar->back_button = ui_info[0].widget;
	toolbar->forward_button = ui_info[1].widget;
	toolbar->up_button = ui_info[2].widget;
	toolbar->reload_button = ui_info[3].widget;
	toolbar->home_button = ui_info[5].widget;
	toolbar->stop_button = ui_info[7].widget;

	explorer_window_toolbar_allow_up (toolbar, FALSE);
	explorer_window_toolbar_allow_back (toolbar, FALSE);
	explorer_window_toolbar_allow_forward (toolbar, FALSE);
	explorer_window_toolbar_allow_stop (toolbar, FALSE);
}


GtkType
explorer_window_toolbar_get_type (void)
{
	static GtkType type = 0;

	if (type == 0) {
		static GtkTypeInfo info = {
			"ExplorerWindowToolbar",
			sizeof (ExplorerWindowToolbar),
			sizeof (ExplorerWindowToolbarClass),
			(GtkClassInitFunc) class_init,
			(GtkObjectInitFunc) init,
			NULL,
			NULL,
			NULL
		};

		type = gtk_type_unique (gtk_toolbar_get_type (), &info);
	}

	return type;
}

ExplorerWindowToolbar *
explorer_window_toolbar_new (ExplorerWindow *window)
{
	ExplorerWindowToolbar *toolbar;

	g_return_val_if_fail (window != NULL, NULL);

	toolbar = gtk_type_new (explorer_window_toolbar_get_type ());
	toolbar->window = window;

	return toolbar;
}


void
explorer_window_toolbar_allow_up (ExplorerWindowToolbar *toolbar,
				  gboolean allow)
{
	g_return_if_fail (toolbar != NULL);
	g_return_if_fail (EXPLORER_IS_TOOLBAR (toolbar));

	gtk_widget_set_sensitive (toolbar->up_button, allow);
}

void
explorer_window_toolbar_allow_back (ExplorerWindowToolbar *toolbar,
				    gboolean allow)
{
	g_return_if_fail (toolbar != NULL);
	g_return_if_fail (EXPLORER_IS_TOOLBAR (toolbar));

	gtk_widget_set_sensitive (toolbar->back_button, allow);
}

void
explorer_window_toolbar_allow_forward (ExplorerWindowToolbar *toolbar,
				       gboolean allow)
{
	g_return_if_fail (toolbar != NULL);
	g_return_if_fail (EXPLORER_IS_TOOLBAR (toolbar));

	gtk_widget_set_sensitive (toolbar->forward_button, allow);
}

void
explorer_window_toolbar_allow_reload (ExplorerWindowToolbar *toolbar,
				      gboolean allow)
{
	g_return_if_fail (toolbar != NULL);
	g_return_if_fail (EXPLORER_IS_TOOLBAR (toolbar));

	gtk_widget_set_sensitive (toolbar->reload_button, allow);
}

void
explorer_window_toolbar_allow_stop (ExplorerWindowToolbar *toolbar,
				    gboolean allow)
{
	g_return_if_fail (toolbar != NULL);
	g_return_if_fail (EXPLORER_IS_TOOLBAR (toolbar));

	gtk_widget_set_sensitive (toolbar->stop_button, allow);
}

