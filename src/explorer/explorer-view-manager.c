/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* explorer-view-manager.c - View manager.

   Copyright (C) 1999 Free Software Foundation

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public
   License along with this program; see the file COPYING.  If not,
   write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.

   Author: Ettore Perazzoli <ettore@gnu.org>
*/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <bonobo/gnome-bonobo.h>

#include "explorer.h"


struct _ExplorerViewManager {
	GnomeContainer *container;
};


ExplorerViewManager *
explorer_view_manager_new (void)
{
	ExplorerViewManager *new;

	new = g_new (ExplorerViewManager, 1);

	new->container = gnome_container_new ();

	return new;
}

void
explorer_view_manager_destroy (ExplorerViewManager *manager)
{
	g_return_if_fail (manager != NULL);

	gnome_object_unref (GNOME_OBJECT (manager->container));

	g_free (manager);
}

GnomeViewFrame *
explorer_view_manager_new_view_for_mime_type (ExplorerViewManager *manager,
					      const gchar *mime_type)
{
	GnomeViewFrame *view_frame;
	GnomeObjectClient *object_client;
	GnomeClientSite *client_site;
	const gchar *goad_id;

	g_return_val_if_fail (manager != NULL, NULL);
	g_return_val_if_fail (mime_type != NULL, NULL);

	/* FIXME TODO we need to implement a component directory.  */

	if (strcmp (mime_type, "text/html") == 0)
		goad_id = "embeddable:explorer-html-component";
	else if (strcmp (mime_type, "text/plain") == 0)
		goad_id = "embeddable:text-plain";
	else
		return NULL;

	EXPLORER_DEBUG (("Activating GOAD ID `%s'\n", goad_id));

	object_client = gnome_object_activate_with_goad_id (NULL, goad_id,
							    0, NULL);
	if (object_client == NULL) {
		g_warning (_("Component for goad id `%s' could not be activated.\n"),
			   goad_id);
		return NULL;
	}

	client_site = gnome_client_site_new (manager->container);
	gnome_container_add (manager->container, GNOME_OBJECT (object_client));

	if (! gnome_client_site_bind_embeddable (client_site, object_client)) {
		g_warning (_("Cannot bind client for `%s' to client site."),
			   mime_type);
		return NULL;
	}

	view_frame = gnome_client_site_new_view (client_site);

	return view_frame;
}

void
explorer_view_manager_destroy_view (ExplorerViewManager *manager,
				    GnomeViewFrame *view_frame)
{
	GnomeClientSite *client_site;

	g_return_if_fail (manager != NULL);
	g_return_if_fail (view_frame != NULL);
	g_return_if_fail (GNOME_IS_VIEW_FRAME (view_frame));

	client_site = gnome_view_frame_get_client_site (view_frame);

	gnome_object_unref (GNOME_OBJECT (view_frame));
	gnome_object_unref (GNOME_OBJECT (client_site));
}
