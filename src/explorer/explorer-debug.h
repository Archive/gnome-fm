/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* explorer-debug.h

   Copyright (C) 1999 Free Software Foundation

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public
   License along with this program; see the file COPYING.  If not,
   write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.

   Author: Ettore Perazzoli <ettore@gnu.org>
*/

#ifndef _EXPLORER_DEBUG_H
#define _EXPLORER_DEBUG_H

#include <glib.h>
#include <stdio.h>

#ifdef EXPLORER_DEBUG_ENABLED

#define EXPLORER_DEBUG(x)						\
G_STMT_START{								\
	printf ("** Explorer (" __FILE__ " " __FUNCTION__ "): ");	\
	printf x;							\
	fputc ('\n', stdout);                                           \
        fflush(stdout);                                                 \
}G_STMT_END

#undef FUNCTION_STRING

#else

#define EXPLORER_DEBUG(x)

#endif

#endif /* _EXPLORER_DEBUG_H */
