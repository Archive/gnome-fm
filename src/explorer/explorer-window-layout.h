/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* explorer-window-layout.h

   Copyright (C) 1999 Free Software Foundation

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public
   License along with this program; see the file COPYING.  If not,
   write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.

   Author: Ettore Perazzoli <ettore@gnu.org>
*/

#ifndef _EXPLORER_WINDOW_LAYOUT_H
#define _EXPLORER_WINDOW_LAYOUT_H

typedef struct _ExplorerWindowLayout ExplorerWindowLayout;

#include "explorer-window.h"

/* FIXME move somewhere else?  */
#define EXPLORER_LAYOUT_FILE_NAME "...directory.ginfo"

struct _ExplorerWindowLayout {
	gint x, y;
	gint width, height;
	ExplorerDirectoryViewMode view_mode;
	GnomeIconContainerLayout *icon_layout;
};


ExplorerWindowLayout *explorer_window_layout_new     (void);
ExplorerWindowLayout *explorer_window_layout_new_from_window
						     (ExplorerWindow *window);
void 		      explorer_window_layout_free    (ExplorerWindowLayout
						              *layout);
gchar 		     *explorer_window_layout_xmlize  (const ExplorerWindowLayout
						              *layout);
ExplorerWindowLayout *explorer_window_layout_dexmlize
						     (const gchar *xml_text);
ExplorerWindowLayout *explorer_window_layout_load    (const GnomeVFSURI *uri);
GnomeVFSResult        explorer_window_layout_save    (const GnomeVFSURI *uri,
						      const ExplorerWindowLayout *layout);

#endif /* _EXPLORER_WINDOW_LAYOUT_H */
