/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* explorer-loader.h
 * Copyright (C) 1999  Free Software Foundaton
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Author: Ettore Perazzoli
 */
#ifndef __EXPLORER_LOADER_H__
#define __EXPLORER_LOADER_H__

#include <bonobo/gnome-bonobo.h>
#include <gnome.h>
#include <libgnomevfs/gnome-vfs.h>
#include <libgnorba/gnorba.h>

#ifdef __cplusplus
extern "C" {
#pragma }
#endif /* __cplusplus */


#define EXPLORER_TYPE_VIEW_LOADER	(explorer_loader_get_type ())
#define EXPLORER_LOADER(obj)		(GTK_CHECK_CAST ((obj), EXPLORER_TYPE_VIEW_LOADER, ExplorerLoader))
#define EXPLORER_LOADER_CLASS(klass)	(GTK_CHECK_CLASS_CAST ((klass), EXPLORER_TYPE_VIEW_LOADER, ExplorerLoaderClass))
#define EXPLORER_IS_LOADER(obj)		(GTK_CHECK_TYPE ((obj), EXPLORER_TYPE_VIEW_LOADER))
#define EXPLORER_IS_LOADER_CLASS(klass)	(GTK_CHECK_CLASS_TYPE ((obj), EXPLORER_TYPE_VIEW_LOADER))


typedef struct _ExplorerLoader       ExplorerLoader;
typedef struct _ExplorerLoaderClass  ExplorerLoaderClass;

struct _ExplorerLoader {
	GtkObject object;

	GnomeVFSAsyncHandle *vfs_async_handle;

	GnomeVFSURI *uri;
	gchar *mime_type;

	GIOChannel *channel;

	CORBA_Object load_interface_objref;
};

struct _ExplorerLoaderClass {
	GtkObjectClass parent_class;

	void (*status_message)  (ExplorerLoader *view_loader,
				 const gchar* message);
	void (*open_failed)	(ExplorerLoader *view_loader,
				 GnomeVFSResult result);
	void (*open_done)	(ExplorerLoader *view_loader,
				 const gchar *mime_type);
	void (*load_failed)	(ExplorerLoader *view_loader,
				 GnomeVFSResult result);
	void (*load_done)	(ExplorerLoader *view_loader);
};


GtkType            explorer_loader_get_type      (void);
ExplorerLoader 	  *explorer_loader_new           (const GnomeVFSURI *uri);

gboolean           explorer_loader_set_progressive_data_sink
						 (ExplorerLoader *loader,
					          GNOME_ProgressiveDataSink pdsink);

gboolean           explorer_loader_set_component (ExplorerLoader *loader,
						  GnomeObject *component);

void		   explorer_loader_stop          (ExplorerLoader *loader);

const GnomeVFSURI *explorer_loader_get_uri	 (ExplorerLoader *loader);
const gchar 	  *explorer_loader_get_mime_type (ExplorerLoader *loader);

void               explorer_loader_status_message(ExplorerLoader *loader,
						  const gchar *message);

#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* __EXPLORER_LOADER_H__ */
