/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* explorer-window-layout.c

   Copyright (C) 1999 Free Software Foundation

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public
   License along with this program; see the file COPYING.  If not,
   write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.

   Author: Ettore Perazzoli <ettore@gnu.org>
*/

#include <gtk/gtk.h>
#include <gnome-xml/parser.h>

#include "explorer-window.h"

#include "explorer-window-layout.h"


ExplorerWindowLayout *
explorer_window_layout_new (void)
{
	ExplorerWindowLayout *layout;

	layout = g_new (ExplorerWindowLayout, 1);

	layout->x = 0;
	layout->y = 0;
	layout->width = 0;
	layout->height = 0;
	layout->view_mode = EXPLORER_DIRECTORY_VIEW_MODE_ICONS;
	layout->icon_layout = NULL;

	return layout;
}

ExplorerWindowLayout *
explorer_window_layout_new_from_window (ExplorerWindow *window)
{
	ExplorerWindowLayout *layout;
	GtkWidget *widget;

	g_return_val_if_fail (window != NULL, NULL);
	g_return_val_if_fail (EXPLORER_IS_WINDOW (window), NULL);

	widget = GTK_WIDGET (window);

	layout = g_new (ExplorerWindowLayout, 1);

	gdk_window_get_root_origin (widget->window, &layout->x, &layout->y);
	gdk_window_get_size (widget->window, &layout->width, &layout->height);

	layout->view_mode = window->directory_view_mode;

	if (window->directory_view == NULL) 
		layout->icon_layout = NULL;
	else
		layout->icon_layout = explorer_directory_view_get_icon_layout
			(window->directory_view);

	return layout;
}

void
explorer_window_layout_free (ExplorerWindowLayout *layout)
{
	g_return_if_fail (layout != NULL);

	if (layout->icon_layout != NULL)
		gnome_icon_container_layout_free (layout->icon_layout);

	g_free (layout);
}


/* Shamelessly stolen from Damon Chaplin's `save.c' implementation in Glade.  */
static void
append_with_conversion (GString *buffer,
			const gchar *string)
{
	gchar ch;

	while ((ch = *string++)) {
		if (ch == '<')
			g_string_append (buffer, "&lt;");
		else if (ch == '>')
			g_string_append (buffer, "&gt;");
		else if (ch == '&')
			g_string_append (buffer, "&amp;");
		else if (ch == '"')
			g_string_append (buffer, "&quot;");
		else
			g_string_append_c (buffer, ch);
	}
}

static void
xmlize_icon_callback (const GnomeIconContainerLayout *layout,
		      const gchar *text,
		      gint x, gint y,
		      gpointer callback_data)
{
	GString *s;

	s = (GString *) callback_data;

	g_string_sprintfa (s, "\t<file x=\"%d\" y=\"%d\">", x, y);
	append_with_conversion (s, text);
	g_string_append (s, "</file>\n");
}

gchar *
explorer_window_layout_xmlize (const ExplorerWindowLayout *layout)
{
	GString *s;
	gchar *retval;

	g_return_val_if_fail (layout != NULL, NULL);

	s = g_string_sized_new (4096);

	g_string_sprintfa (s, "<?xml version=\"1.0\"?>\n\n");
	g_string_sprintfa (s,
			   "<gnome-directory-layout "
			   "x=\"%d\" y=\"%d\" width=\"%d\" height=\"%d\">\n\n",
			   layout->x, layout->y, layout->width, layout->height);

	gnome_icon_container_layout_foreach (layout->icon_layout,
					     xmlize_icon_callback,
					     s);

	g_string_sprintfa (s, "\n</gnome-directory-layout>\n");

	retval = s->str;
	g_string_free (s, FALSE);

	return retval;
}


/* Our SAX-based XML parser for the layout information.  */

enum _LayoutParserState {
	PARSER_START,
	PARSER_FINISH,
	PARSER_LAYOUT,
	PARSER_FILE,
	PARSER_UNKNOWN
};
typedef enum _LayoutParserState LayoutParserState;

struct _LayoutParsingState {
	LayoutParserState state;
	LayoutParserState prev_state;

	guint unknown_depth;
	ExplorerWindowLayout *layout;

	GString *file_name;
	gboolean valid_x, valid_y;
	gint file_x, file_y;
};
typedef struct _LayoutParsingState LayoutParsingState;

static xmlEntityPtr
get_entity (LayoutParsingState *state, const CHAR *name)
{
	return xmlGetPredefinedEntity (name);
}

static void
start_document (LayoutParsingState *state)
{
	state->state = PARSER_START;
	state->prev_state = PARSER_UNKNOWN;

	state->unknown_depth = 0;
	state->file_name = NULL;

	state->file_x = 0;
	state->file_y = 0;
	state->valid_x = FALSE;
	state->valid_y = FALSE;

	state->layout = explorer_window_layout_new ();
}

static void
end_document (LayoutParsingState *state)
{
	if (state->unknown_depth != 0)
		g_warning ("unknown_depth != 0 (%d)", state->unknown_depth);
}

#define HANDLE_UNKNOWN_TAG(state)		\
G_STMT_START{					\
	(state)->prev_state = (state)->state;	\
	(state)->state = PARSER_UNKNOWN;	\
	(state)->unknown_depth++;		\
}G_STMT_END

static void
start_element (LayoutParsingState *state,
	       const CHAR *name,
	       const CHAR **attrs)
{
	switch (state->state) {
	case PARSER_START:
		if (strcmp (name, "gnome-directory-layout") != 0) {
			g_warning ("Expecting `gnome-directory-layout', got `%s'.",
				   name);
			HANDLE_UNKNOWN_TAG (state);
		} else {
			const CHAR **p;

			for (p = attrs; *p != NULL; p += 2) {
				const CHAR *name;
				const CHAR *value;

				name = p[0], value = p[1];
				if (strcmp (name, "x") == 0)
					state->layout->x = atoi (value);
				else if (strcmp (name, "y") == 0)
					state->layout->y = atoi (value);
				else if (strcmp (name, "width") == 0)
					state->layout->width = atoi (value);
				else if (strcmp (name, "height") == 0)
					state->layout->height = atoi (value);
			}

			state->state = PARSER_LAYOUT;
		}
		break;
	case PARSER_LAYOUT:
		if (strcmp (name, "file") != 0) {
			g_warning ("Expecting `file', got `%s'.", name);
			HANDLE_UNKNOWN_TAG (state);
		} else {
			const CHAR **p;

			state->valid_x = FALSE;
			state->valid_y = FALSE;

			for (p = attrs; *p != NULL; p += 2) {
				const CHAR *name;
				const CHAR *value;

				name = p[0], value = p[1];
				if (strcmp (name, "x") == 0) {
					state->file_x = atoi (value);
					state->valid_x = TRUE;
				} else if (strcmp (name, "y") == 0) {
					state->file_y = atoi (value);
					state->valid_y = TRUE;
				}
			}
			state->state = PARSER_FILE;
		}
		break;
	case PARSER_FILE:
		g_warning ("Unexpected token `%s' within `file'.", name);
		HANDLE_UNKNOWN_TAG (state);
		break;
	case PARSER_UNKNOWN:
		state->unknown_depth++;
		break;
	case PARSER_FINISH:
		/* This should never happen.  */
		g_assert_not_reached ();
		break;
	}
}

static void
end_element (LayoutParsingState *state,
	     const CHAR *name)
{
	/* FIXME? */
	switch (state->state) {
	case PARSER_LAYOUT:
		break;
	case PARSER_FILE:
		if (strcmp (name, "file") == 0) {
			if (state->valid_x && state->valid_y) {
				if (state->layout->icon_layout == NULL)
					state->layout->icon_layout
						= gnome_icon_container_layout_new ();
				gnome_icon_container_layout_add
					(state->layout->icon_layout,
					 state->file_name->str,
					 state->file_x,
					 state->file_y);
			}
			g_string_free (state->file_name, TRUE);
			state->file_name = NULL;
			state->state = PARSER_LAYOUT;
		}
		break;
	case PARSER_UNKNOWN:
		state->unknown_depth--;
		if (state->unknown_depth == 0)
			state->state = state->prev_state;
		break;
	case PARSER_START:
	case PARSER_FINISH:
		/* This should never happen.  */
		g_assert_not_reached ();
		break;
	}
}

static void
characters (LayoutParsingState *state,
	    const CHAR *chars,
	    int len)
{
	if (state->state == PARSER_FILE) {
		CHAR *tmp;

		if (state->file_name == NULL)
			state->file_name = g_string_new (NULL);

		tmp = alloca (sizeof (CHAR) * len + 1);
		memcpy (tmp, chars, len);
		tmp[len] = 0;
		g_string_append (state->file_name, tmp);
	}
}

static void
warning (LayoutParsingState *state,
	 const char *msg, ...)
{
	va_list args;

	va_start(args, msg);
	g_logv("XML", G_LOG_LEVEL_WARNING, msg, args);
	va_end(args);
}

static void
error (LayoutParsingState *state,
       const char *msg,
       ...)
{
	va_list args;

	va_start(args, msg);
	g_logv("XML", G_LOG_LEVEL_CRITICAL, msg, args);
	va_end(args);
}

static void
fatal_error (LayoutParsingState *state,
	     const char *msg,
	     ...)
{
	va_list args;

	va_start(args, msg);
	g_logv("XML", G_LOG_LEVEL_ERROR, msg, args);
	va_end(args);
}

static xmlSAXHandler parser = {
	0, /* internalSubset */
	0, /* isStandalone */
	0, /* hasInternalSubset */
	0, /* hasExternalSubset */
	0, /* resolveEntity */
	(getEntitySAXFunc) get_entity, /* getEntity */
	0, /* entityDecl */
	0, /* notationDecl */
	0, /* attributeDecl */
	0, /* elementDecl */
	0, /* unparsedEntityDecl */
	0, /* setDocumentLocator */
	(startDocumentSAXFunc) start_document, /* startDocument */
	(endDocumentSAXFunc) end_document, /* endDocument */
	(startElementSAXFunc) start_element, /* startElement */
	(endElementSAXFunc) end_element, /* endElement */
	0, /* reference */
	(charactersSAXFunc) characters, /* characters */
	0, /* ignorableWhitespace */
	0, /* processingInstruction */
	(commentSAXFunc)0, /* comment */
	(warningSAXFunc) warning, /* warning */
	(errorSAXFunc) error, /* error */
	(fatalErrorSAXFunc) fatal_error, /* fatalError */
};

ExplorerWindowLayout *
explorer_window_layout_dexmlize (const gchar *xml_text)
{
	LayoutParsingState state;

	g_return_val_if_fail (xml_text != NULL, NULL);

	/* FIXME this should be made const-safe in gnome-xml.  */
	xmlSAXUserParseMemory (&parser, &state,
			       (char *) xml_text, strlen (xml_text));

#if 0
	if (state.file_name != NULL)
		g_string_free (state.file_name, TRUE);
#endif

	return state.layout;
}


/* FIXME we should make this async as well, even though we will be using this
   for local file systems only (NFS might block).  */
ExplorerWindowLayout *
explorer_window_layout_load (const GnomeVFSURI *uri)
{
#define BUFFER_SIZE 4096
	ExplorerWindowLayout *layout;
	GnomeVFSURI *layout_file_uri;
	GnomeVFSResult result;
	gchar buffer[BUFFER_SIZE];
	GByteArray *data;
	GnomeVFSHandle *handle;

	g_return_val_if_fail (uri != NULL, NULL);
	g_return_val_if_fail (gnome_vfs_uri_is_local (uri), NULL);

	layout_file_uri = gnome_vfs_uri_append_path
		(uri, EXPLORER_LAYOUT_FILE_NAME);
	if (layout_file_uri == NULL)
		return NULL;

	result = gnome_vfs_open_uri (&handle, layout_file_uri, GNOME_VFS_OPEN_READ);
	gnome_vfs_uri_unref (layout_file_uri);

	if (result != GNOME_VFS_OK)
		return NULL;

	data = g_byte_array_new ();

	while (1) {
		GnomeVFSFileSize bytes_read;

		result = gnome_vfs_read (handle, buffer, sizeof (buffer),
					 &bytes_read);
		if (bytes_read == 0 || result != GNOME_VFS_OK)
			break;

		g_byte_array_append (data, buffer, bytes_read);
	}

	gnome_vfs_close (handle);

	layout = explorer_window_layout_dexmlize (data->data);

	g_byte_array_free (data, TRUE);

	return layout;

#undef BUFFER_SIZE
}

/* FIXME we should make this async as well, even though we will be using this
   for local file systems only (NFS might block).  */
/* FIXME we need to handle conflicts, i.e. basically create the file in a
   temporary location and then rename it atomically.  */
GnomeVFSResult
explorer_window_layout_save (const GnomeVFSURI *uri,
			     const ExplorerWindowLayout *layout)
{
	GnomeVFSURI *layout_file_uri;
	GnomeVFSHandle *handle;
	GnomeVFSResult result;
	gchar *xml;
	gchar *p;
	guint size;

	g_return_val_if_fail (uri != NULL, GNOME_VFS_ERROR_BADPARAMS);
	g_return_val_if_fail (layout != NULL, GNOME_VFS_ERROR_BADPARAMS);
	g_return_val_if_fail (gnome_vfs_uri_is_local (uri), GNOME_VFS_ERROR_BADPARAMS);

	xml = explorer_window_layout_xmlize (layout);
	if (xml == NULL)
		return GNOME_VFS_OK;

	layout_file_uri = gnome_vfs_uri_append_path
		(uri, EXPLORER_LAYOUT_FILE_NAME);
	if (layout_file_uri == NULL) {
		g_free (xml);
		return GNOME_VFS_ERROR_INTERNAL;
	}

	result = gnome_vfs_create_uri (&handle, layout_file_uri,
				       GNOME_VFS_OPEN_WRITE,
				       FALSE, 0600);
	gnome_vfs_uri_unref (layout_file_uri);

	if (result != GNOME_VFS_OK) {
		g_free (xml);
		return result;
	}

	p = xml;
	size = strlen (xml);
	while (size > 0) {
		GnomeVFSFileSize bytes_written;

		result = gnome_vfs_write (handle, p, size, &bytes_written);
		if (result != GNOME_VFS_OK)
			break;
		p += (guint) bytes_written;
		size -= (guint) bytes_written;
	}

	gnome_vfs_close (handle);
	g_free (xml);

	return result;
}
