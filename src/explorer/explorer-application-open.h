/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* explorer-application-open.h - Implementation of the Explorer's actions.

   Copyright (C) 1999 Free Software Foundation

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public
   License along with this program; see the file COPYING.  If not,
   write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.

   Author: Ettore Perazzoli <ettore@gnu.org>
*/

#ifndef _EXPLORER_APPLICATION_ACTIONS_H
#define _EXPLORER_APPLICATION_ACTIONS_H

#include <libgnomevfs/gnome-vfs.h>

enum _ExplorerApplicationOpenOptions {
	EXPLORER_APPLICATION_OPEN_DEFAULT = 0,
	/* Force the URI to be open in the same window without respecting the
           browsing settings.  */
	EXPLORER_APPLICATION_OPEN_SAMEWINDOW = 1 << 0,
	/* Force the URI to be open in a new window without respecting the
           browsing settings.  */
	EXPLORER_APPLICATION_OPEN_NEWWINDOW = 1 << 1
};
typedef enum _ExplorerApplicationOpenOptions ExplorerApplicationOpenOptions;

#include "explorer-application.h"
#include "explorer-window.h"

void explorer_application_open (ExplorerApplication *application,
				const GnomeVFSURI *uri,
				ExplorerWindow *src_window,
				const gchar *mime_type,
				const gchar *action_id,
				ExplorerApplicationOpenOptions options);

#endif /*  _EXPLORER_APPLICATION_ACTIONS_H */
