/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* explorer-view-manager.h - View management.

   Copyright (C) 1999 Free Software Foundation

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public
   License along with this program; see the file COPYING.  If not,
   write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.

   Author: Ettore Perazzoli <ettore@gnu.org>
*/

#ifndef _EXPLORER_VIEW_MANAGER_H
#define _EXPLORER_VIEW_MANAGER_H

#include <bonobo/gnome-view-frame.h>
#include <bonobo/gnome-object-client.h>

typedef struct _ExplorerViewManager ExplorerViewManager;


ExplorerViewManager	*explorer_view_manager_new
					(void);
void			 explorer_view_manager_destroy
					(ExplorerViewManager *manager);

GnomeViewFrame 		*explorer_view_manager_new_view_for_mime_type
					(ExplorerViewManager *manager,
					 const gchar *mime_type);
void			 explorer_view_manager_destroy_view
					(ExplorerViewManager *manager,
					 GnomeViewFrame *view_frame);

#endif /* _EXPLORER_VIEW_MANAGER_H */
