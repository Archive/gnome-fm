/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* explorer-icon-manager.h

   Copyright (C) 1999 Free Software Foundation

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public
   License along with this program; see the file COPYING.  If not,
   write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.

   Authors: Ettore Perazzoli <ettore@gnu.org>
            Miguel de Icaza <miguel@gnu.org>
            Federico Mena Quintero <federico@redhat.com>
*/

#ifndef __EXPLORER_ICON_MANAGER_H
#define __EXPLORER_ICON_MANAGER_H

#include <gdk_imlib.h>
#include <glib.h>
#include <libgnomevfs/gnome-vfs.h>

typedef struct _ExplorerIconManager ExplorerIconManager;

#include "explorer-application.h"


ExplorerIconManager *explorer_icon_manager_new  (ExplorerApplication
						 	*application);
void		     explorer_icon_manager_destroy
						(ExplorerIconManager *manager);
GdkImlibImage       *explorer_icon_manager_get_icon_for_info
						(ExplorerIconManager *manager,
						 const GnomeVFSFileInfo *info);

#endif /* __EXPLORER_ICON_MANAGER_H */
