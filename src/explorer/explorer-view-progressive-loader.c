/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* explorer-view-progressive-loader.c
 *
 * Copyright (C) 1999  Free Software Foundaton
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Author: Ettore Perazzoli
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gnome.h>

#include "explorer.h"

#include "explorer-view-progressive-loader.h"


static GnomeProgressiveLoaderClass *parent_class = NULL;

struct _LoaderData {
	ExplorerViewProgressiveLoader *parent;
	ExplorerLoader *loader;
	GNOME_ProgressiveDataSink pdsink;
};
typedef struct _LoaderData LoaderData;


static void
loader_data_free (LoaderData *data)
{
	CORBA_Environment ev;

	CORBA_exception_init (&ev);
	CORBA_Object_release (data->pdsink, &ev);
	CORBA_exception_free (&ev);

	gtk_object_destroy (GTK_OBJECT (data->loader));
	g_free (data);
}

static void
remove_loader (ExplorerViewProgressiveLoader *loader,
	       LoaderData *data)
{
	GList *p, *pnext;

	for (p = loader->loaders; p != NULL; p = pnext) {
		LoaderData *d;

		pnext = p->next;
		d = p->data;
		if (data == d) {
			loader->loaders = g_list_remove_link (loader->loaders,
							      p);
			loader_data_free (d);
			return;
		}
	}

	g_warning ("ProgressiveLoader::remove_loader -- loader not found in list.");
}


/* ExplorerLoader signals.  */

static void
open_failed_cb (ExplorerLoader *view_loader,
		GnomeVFSResult result,
		gpointer data)
{
	LoaderData *loader_data;
	ExplorerViewProgressiveLoader *parent;
	CORBA_Environment ev;

	loader_data = (LoaderData *) data;
	parent = loader_data->parent;

	CORBA_exception_init (&ev);

	/* FIXME there must be a better way to report error.  */
	GNOME_ProgressiveDataSink_start (loader_data->pdsink, &ev);
	GNOME_ProgressiveDataSink_end (loader_data->pdsink, &ev);

	CORBA_exception_free (&ev);

	remove_loader (loader_data->parent, loader_data);
}

static void
open_done_cb (ExplorerLoader *loader,
	      const gchar *mime_type,
	      gpointer data)
{
	LoaderData *loader_data;
	ExplorerViewProgressiveLoader *parent;

	loader_data = (LoaderData *) data;
	parent = loader_data->parent;

	if (! explorer_loader_set_progressive_data_sink (loader,
							 loader_data->pdsink)) {
		g_warning ("ExplorerViewProgressiveLoader: Cannot set the ProgressiveDataSink interface\n");
		remove_loader (loader_data->parent, loader_data);
	}
}

static void
load_failed_cb (ExplorerLoader *loader,
		GnomeVFSResult result,
		gpointer data)
{
	LoaderData *loader_data;
	ExplorerViewProgressiveLoader *parent;
	CORBA_Environment ev;

	loader_data = (LoaderData *) data;
	parent = loader_data->parent;

	CORBA_exception_init (&ev);

	/* FIXME: how to report error?  */
	GNOME_ProgressiveDataSink_end (loader_data->pdsink, &ev);

	CORBA_exception_free (&ev);

	remove_loader (loader_data->parent, loader_data);
}

static void
load_done_cb (ExplorerLoader *loader,
	      gpointer data)
{
	ExplorerViewProgressiveLoader *parent;
	LoaderData *loader_data;

	loader_data = (LoaderData *) data;
	parent = loader_data->parent;

	parent->loaders = g_list_remove (parent->loaders, loader_data);
}

static void
destroy_cb (ExplorerLoader *loader,
	    gpointer data)
{
	LoaderData *loader_data;

	loader_data = (LoaderData *) data;

	remove_loader (loader_data->parent, loader_data);
}

static void
setup_explorer_loader_signals (ExplorerLoader *loader,
			       LoaderData *data)
{
	gtk_signal_connect (GTK_OBJECT (loader), "open_done",
			    GTK_SIGNAL_FUNC (open_done_cb), data);
	gtk_signal_connect (GTK_OBJECT (loader), "open_failed",
			    GTK_SIGNAL_FUNC (open_failed_cb), data);
	gtk_signal_connect (GTK_OBJECT (loader), "load_done",
			    GTK_SIGNAL_FUNC (load_done_cb), data);
	gtk_signal_connect (GTK_OBJECT (loader), "load_failed",
			    GTK_SIGNAL_FUNC (load_failed_cb), data);
	gtk_signal_connect (GTK_OBJECT (loader), "destroy",
			    GTK_SIGNAL_FUNC (destroy_cb), data);
}


/* GnomeProgressiveLoader `load' function.  */

static GnomeVFSResult
load (GnomeProgressiveLoader *gnome_progressive_loader,
      const gchar *text_uri,
      GNOME_ProgressiveDataSink pdsink)
{
	ExplorerLoader *explorer_loader;
	ExplorerViewProgressiveLoader *explorer_view_progressive_loader;
	GNOME_ProgressiveDataSink pdsink_dup;
	LoaderData *loader_data;
	CORBA_Environment ev;
	GnomeVFSURI *uri;

	CORBA_exception_init (&ev);

	pdsink_dup = CORBA_Object_duplicate (pdsink, &ev);
	if (ev._major != CORBA_NO_EXCEPTION) {
		CORBA_exception_free (&ev);
		return GNOME_VFS_ERROR_INTERNAL;
	}

	explorer_view_progressive_loader
		= EXPLORER_VIEW_PROGRESSIVE_LOADER (gnome_progressive_loader);

	uri = gnome_vfs_uri_new (text_uri);
	explorer_loader = explorer_loader_new (uri);
	gnome_vfs_uri_unref (uri);

	loader_data = g_new (LoaderData, 1);
	loader_data->parent = explorer_view_progressive_loader;
	loader_data->loader = explorer_loader;
	loader_data->pdsink = pdsink_dup;

	setup_explorer_loader_signals (explorer_loader, loader_data);
	
	explorer_view_progressive_loader->loaders
		= g_list_prepend (explorer_view_progressive_loader->loaders,
				  loader_data);

	CORBA_exception_free (&ev);

	return GNOME_VFS_OK;
}


/* GtkObject methods.  */

static void
destroy (GtkObject *object)
{
	ExplorerViewProgressiveLoader *loader;
	GList *p;

	loader = EXPLORER_VIEW_PROGRESSIVE_LOADER (object);

	for (p = loader->loaders; p != NULL; p = p->next)
		loader_data_free ((LoaderData *) p->data);

	g_list_free (loader->loaders);

	if (GTK_OBJECT_CLASS (parent_class)->destroy != NULL)
		(* GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}


/* Initialization.  */

static void
class_init (ExplorerViewProgressiveLoaderClass *class)
{
	GtkObjectClass *object_class;

	object_class = GTK_OBJECT_CLASS (class);
	object_class->destroy = destroy;

	parent_class = gtk_type_class (gnome_progressive_loader_get_type ());
}

static void
init (ExplorerViewProgressiveLoader *loader)
{
	loader->loaders = NULL;
}


GtkType
explorer_view_progressive_loader_get_type (void)
{
	static GtkType type = 0;

	if (type == 0) {
		static const GtkTypeInfo info = {
			"ExplorerViewProgressiveLoader",
			sizeof (ExplorerViewProgressiveLoader),
			sizeof (ExplorerViewProgressiveLoaderClass),
			(GtkClassInitFunc) class_init,
			(GtkObjectInitFunc) init,
			/* reserved_1 */ NULL,
			/* reserved_2 */ NULL,
			(GtkClassInitFunc) NULL,
		};

		type = gtk_type_unique (gnome_progressive_loader_get_type (), 
					&info);
	}

	return type;
}

void
explorer_view_progressive_loader_construct (ExplorerViewProgressiveLoader *loader)
{
	gnome_progressive_loader_construct (GNOME_PROGRESSIVE_LOADER (loader),
					    CORBA_OBJECT_NIL, load);
}

ExplorerViewProgressiveLoader *
explorer_view_progressive_loader_new (void)
{
	ExplorerViewProgressiveLoader *loader;

	loader = gtk_type_new (explorer_view_progressive_loader_get_type ());
	explorer_view_progressive_loader_construct (loader);

	return loader;
}
