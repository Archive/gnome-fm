/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* explorer-window.c - Explorer window for the GNOME Explorer.

   Copyright (C) 1999 Free Software Foundation

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public
   License along with this program; see the file COPYING.  If not,
   write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.

   Author: Ettore Perazzoli <ettore@gnu.org>
*/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gnome.h>

#include "error.h"

#include "explorer-application-open.h"
#include "explorer-constants.h"
#include "explorer-debug.h"
#include "explorer-directory-view.h"
#include "explorer-history.h"
#include "explorer-location-bar.h"
#include "explorer-window-toolbar.h"

#include "explorer-window.h"


#define DEFAULT_WIDTH	450
#define DEFAULT_HEIGHT	400


static GnomeAppClass *parent_class = NULL;


/* Utility functions.  */

static void
update_gui_controls (ExplorerWindow *window)
{
	if (window->uri == NULL) {
		explorer_location_bar_set_uri_string
			(EXPLORER_LOCATION_BAR (window->location_bar), NULL);
		explorer_window_toolbar_allow_up (window->toolbar, FALSE);
		explorer_window_toolbar_allow_reload (window->toolbar, FALSE);
	} else {
		gchar *uri_string;

		uri_string = gnome_vfs_uri_to_string
			(window->uri, GNOME_VFS_URI_HIDE_PASSWORD);
		explorer_location_bar_set_uri_string
			(EXPLORER_LOCATION_BAR (window->location_bar),
			 uri_string);
		g_free (uri_string);

		explorer_window_toolbar_allow_up
			(window->toolbar,
			 gnome_vfs_uri_has_parent (window->uri));
		explorer_window_toolbar_allow_reload (window->toolbar, TRUE);
	}

	explorer_window_toolbar_allow_back
		(window->toolbar, explorer_history_can_back (window->history));

	explorer_window_toolbar_allow_forward
		(window->toolbar, explorer_history_can_forward (window->history));

	if (window->status == EXPLORER_WINDOW_STATUS_IDLE)
		explorer_window_toolbar_allow_stop (window->toolbar, FALSE);
	else
		explorer_window_toolbar_allow_stop (window->toolbar, TRUE);
}

static void
set_contents (ExplorerWindow *window,
	      GtkWidget *widget)
{
	GtkBin *bin;

	bin = GTK_BIN (window->frame);

	if (bin->child != NULL)
		gtk_widget_destroy (bin->child);

	if (widget != NULL) {
		gtk_container_add (GTK_CONTAINER (bin), widget);
		gtk_widget_show (widget);
	}
}

static void
save_layout (ExplorerWindow *window)
{
	if (window->uri != NULL && gnome_vfs_uri_is_local (window->uri)) {
		GnomeVFSResult result;
		ExplorerWindowLayout *layout;

		/* FIXME make this stuff asynchronous as well.  */

		layout = explorer_window_get_layout (window);

		result = explorer_window_layout_save (window->uri, layout);
		if (result != GNOME_VFS_OK)
			/* FIXME */
			g_warning ("Error saving layout information: %s",
				   gnome_vfs_result_to_string (result));
		else
			EXPLORER_DEBUG (("Layout information saved correctly."));

		explorer_window_layout_free (layout);
	}
}


/* Status updating functions.  */

static void
start_load (ExplorerWindow *window)
{
	/* FIXME report to the statusbar or something...  */

	window->status = EXPLORER_WINDOW_STATUS_LOADING;

	if (window->uri != NULL)
		gnome_vfs_uri_unref (window->uri);
	window->uri = window->new_uri;

	if (window->add_new_uri_to_history) {
		explorer_history_add (window->history, window->uri);
		window->add_new_uri_to_history = FALSE;
	}

	if (window->layout != NULL)
		explorer_window_layout_free (window->layout);

	/* Now, we want the following to happen: if the user opens a directory
	   in a new window, the new window should respect the saved layout.
	   But if it opens a directory in the existing window, the size should
	   not change and icons should be laid out in the default mode.
	   (FIXME: actually, I would like it to load the layout anyway, but
	   only use the order, not the positions.  Easy to do, but not very
	   high priority for now.)

	   From an implementation viewpoint, this is achieved by using default
	   mode when loading in a window that is already been shown, and the
	   layout in a window that is not shown yet.  This saves us from a few
	   annoying extra calls and fits nicely in the GTK+ scheme.  */

	if (GTK_WIDGET_VISIBLE (window)) {
		window->layout = NULL;
	} else{
		window->layout = explorer_window_layout_load (window->uri);
		if (window->layout != NULL) {
			/* FIXME if the window does not fit into the screen
			   (e.g. because user has changed resolution across
			   sessions), maybe we should try to make sure it is
			   always visible.  */
			gtk_widget_set_uposition (GTK_WIDGET (window),
						  window->layout->x,
						  window->layout->y);
			gtk_window_set_default_size (GTK_WINDOW (window),
						     window->layout->width,
						     window->layout->height);
		}
	}

	window->new_uri = NULL;
	window->status = EXPLORER_WINDOW_STATUS_LOADING;

	update_gui_controls (window);
}

static void
start_open (ExplorerWindow *window)
{
	window->status = EXPLORER_WINDOW_STATUS_OPENING;
	update_gui_controls (window);
}

static void
end_load (ExplorerWindow *window)
{
	if (window->loader != NULL) {
		gtk_object_destroy (GTK_OBJECT (window->loader));
		window->loader = NULL;
	}

	if (window->new_uri != NULL) {
		gnome_vfs_uri_unref (window->new_uri);
		window->new_uri = NULL;
	}

	window->status = EXPLORER_WINDOW_STATUS_IDLE;
	update_gui_controls (window);
}


static void
location_changed_cb (ExplorerLocationBar *bar,
		     gpointer data)
{
	ExplorerWindow *window;
	GnomeVFSURI *uri;
	gboolean is_null;

	window = EXPLORER_WINDOW (data);

	uri = explorer_location_bar_get_uri (bar, &is_null);

	if (is_null)
		return;

	if (uri == NULL) {
		error (GTK_WIDGET (window),
		       _("The specified address is not valid."));
		return;
	}

	explorer_application_open (window->application, uri, window,
				   NULL, NULL,
				   EXPLORER_APPLICATION_OPEN_SAMEWINDOW);

	gnome_vfs_uri_unref (uri);
}


static void
directory_activate_uri_cb (ExplorerDirectoryView *view,
			   GnomeVFSURI *uri,
			   const gchar *mime_type,
			   gpointer data)
{
	ExplorerWindow *window;

	EXPLORER_DEBUG (("Entering function"));

	window = EXPLORER_WINDOW (data);

	explorer_application_open (view->application, uri, window,
				   mime_type, NULL,
				   EXPLORER_APPLICATION_OPEN_DEFAULT);
}

static void
directory_open_failed_cb (ExplorerDirectoryView *view,
			  GnomeVFSResult result,
			  gpointer data)
{
	ExplorerWindow *window;
	gchar *uri_string;

	EXPLORER_DEBUG (("Entering function"));

	window = EXPLORER_WINDOW (data);

	uri_string = gnome_vfs_uri_to_string (window->new_uri,
					      GNOME_VFS_URI_HIDE_PASSWORD);
	error (GTK_WIDGET (window), _("Cannot open `%s':\n%s"), uri_string,
	       gnome_vfs_result_to_string (result));
	g_free (uri_string);

	end_load (window);
}

static void
directory_open_done_cb (ExplorerDirectoryView *view,
			gpointer data)
{
	ExplorerWindow *window;

	EXPLORER_DEBUG (("Entering function"));

	window = EXPLORER_WINDOW (data);

	start_load (window);

	set_contents (window, GTK_WIDGET (view));

	gtk_signal_connect (GTK_OBJECT (view), "activate_uri",
			    GTK_SIGNAL_FUNC (directory_activate_uri_cb),
			    window);

	if (window->layout != NULL)
		explorer_directory_view_set_icon_layout
			(view, window->layout->icon_layout);

	/* Now we have the layout, so we can show the window if not shown
           already.  */
	gtk_widget_show (GTK_WIDGET (window));
}

static void
directory_load_failed_cb (ExplorerDirectoryView *view,
			  GnomeVFSResult result,
			  gpointer data)
{
	ExplorerWindow *window;
	gchar *uri_string;

	EXPLORER_DEBUG (("Entering function"));

	window = EXPLORER_WINDOW (data);

	uri_string = gnome_vfs_uri_to_string (window->uri,
					      GNOME_VFS_URI_HIDE_PASSWORD);

	error (GTK_WIDGET (window), _("Error reading `%s':\n%s"),
	       uri_string, gnome_vfs_result_to_string (result));

	g_free (uri_string);

	end_load (window);

	return;
}

static void
directory_load_done_cb (ExplorerDirectoryView *view,
			gpointer data)
{
	ExplorerWindow *window;

	EXPLORER_DEBUG (("Entering function"));

	window = EXPLORER_WINDOW (data);

	end_load (window);
}


static void
load_done_cb (ExplorerLoader *loader,
	      gpointer data)
{
	ExplorerWindow *window;

	EXPLORER_DEBUG (("Entering function"));

	window = EXPLORER_WINDOW (data);
	end_load (window);
}


static void
status_message_cb (ExplorerLoader *loader,
		   const gchar *message,
		   gpointer data)
{
	ExplorerWindow *window;

	window = EXPLORER_WINDOW (data);

	printf("setting statusbar: %s\n", message);

	gnome_appbar_set_status(GNOME_APPBAR(window->app_bar),
				message);
}


/* GtkObject methods.  */

static void
destroy (GtkObject *object)
{
	ExplorerWindow *window;

	EXPLORER_DEBUG (("Entering function."));

	window = EXPLORER_WINDOW (object);

	end_load (window);

	explorer_history_destroy (window->history);

	if (window->loader != NULL)
		gtk_object_destroy (GTK_OBJECT (window->loader));

	if (window->uri != NULL)
		gnome_vfs_uri_unref (window->uri);
	if (window->new_uri != NULL)
		gnome_vfs_uri_unref (window->new_uri);

	if (window->directory_view != NULL)
		gtk_widget_destroy (GTK_WIDGET (window->directory_view));

	if (window->layout != NULL)
		explorer_window_layout_free (window->layout);

	if (window->menu_handler != NULL)
		explorer_window_menu_handler_destroy (window->menu_handler);

	if (GTK_OBJECT_CLASS (parent_class)->destroy != NULL)
		(* GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}


/* GtkWidget methods.  */
static gint
delete_event (GtkWidget *widget,
	      GdkEventAny *event)
{
	ExplorerWindow *window;

	window = EXPLORER_WINDOW (widget);

	/* FIXME: what if this happens in the middle of loading?  We shouldn't
           save anything in that case.  */

	save_layout (window);

	if (GTK_WIDGET_CLASS (parent_class)->delete_event != NULL)
		return (* GTK_WIDGET_CLASS (parent_class)->delete_event)
			(widget, event);

	gtk_widget_destroy (widget);

	return TRUE;
}


/* Initialization.  */

static void
class_init (ExplorerWindowClass *class)
{
	GtkObjectClass *object_class;
	GtkWidgetClass *widget_class;

	parent_class = gtk_type_class (gnome_app_get_type ());

	object_class = GTK_OBJECT_CLASS (class);
	widget_class = GTK_WIDGET_CLASS (class);

	object_class->destroy = destroy;

	widget_class->delete_event = delete_event;
}

static void
init (ExplorerWindow *window)
{
	window->status = EXPLORER_WINDOW_STATUS_IDLE;

	window->uri = NULL;
	window->loader = NULL;
	window->new_uri = NULL;

	window->layout = NULL;

	window->directory_view = NULL;
	window->directory_view_mode = EXPLORER_DIRECTORY_VIEW_MODE_NONE;

	window->history = explorer_history_new ();

	window->frame = NULL;
	window->toolbar = NULL;
	window->location_bar = NULL;
	window->app_bar = NULL;
	window->menu_handler = NULL;
}


GtkType
explorer_window_get_type (void)
{
	static GtkType type = 0;

	if (type == 0) {
		static GtkTypeInfo info = {
			"ExplorerWindow",
			sizeof (ExplorerWindow),
			sizeof (ExplorerWindowClass),
			(GtkClassInitFunc) class_init,
			(GtkObjectInitFunc) init,
			NULL,
			NULL,
			NULL
		};

		type = gtk_type_unique (gnome_app_get_type (), &info);
	}

	return type;
}

/**
 * explorer_window_new:
 * @application: The ExplorerApplication for which the new window will be
 * created.
 * 
 * Create a new window for use in @application.  Normally, you should never
 * call this function directly; instead, use
 * %explorer_application_new_window.
 * 
 * Return value: A pointer to the new widget.
 **/
GtkWidget *
explorer_window_new (ExplorerApplication *application)
{
	ExplorerWindow *new;
	GnomeApp *app;
	GtkWidget *frame;
	GtkWidget *location_bar;
	GtkWidget *app_bar;
	ExplorerWindowToolbar *toolbar;

	g_return_val_if_fail (application != NULL, NULL);

	new = gtk_type_new (explorer_window_get_type ());

	new->application = application;

	/* GnomeApp initialization.  */

	app = GNOME_APP (new);
	gnome_app_construct (app, EXPLORER_APPLICATION_NAME,
			     EXPLORER_APPLICATION_TITLE);
	gnome_app_enable_layout_config (app, FALSE);

	frame = gtk_frame_new (NULL);
	gtk_frame_set_shadow_type (GTK_FRAME (frame), GTK_SHADOW_NONE);
	gtk_widget_show (frame);
	gnome_app_set_contents (app, frame);
	new->frame = frame;

	toolbar = explorer_window_toolbar_new (new);
	gnome_app_set_toolbar (app, GTK_TOOLBAR (toolbar));
	gtk_widget_show (GTK_WIDGET (toolbar));
	new->toolbar = toolbar;

	location_bar = explorer_location_bar_new ();
	gnome_app_add_docked (app, location_bar, "LocationBar",
			      (GNOME_DOCK_ITEM_BEH_EXCLUSIVE
			       | GNOME_DOCK_ITEM_BEH_NEVER_FLOATING
			       | GNOME_DOCK_ITEM_BEH_NEVER_VERTICAL),
			      GNOME_DOCK_TOP,
			      2, 0, 0);
	gtk_widget_show (location_bar);
	new->location_bar = location_bar;

	gtk_signal_connect (GTK_OBJECT (location_bar),
			    "location_changed",
			    GTK_SIGNAL_FUNC (location_changed_cb),
			    new);

	app_bar = gnome_appbar_new (TRUE, TRUE, GNOME_PREFERENCES_NEVER);
	gnome_app_set_statusbar (GNOME_APP (new), app_bar);
	new->app_bar = app_bar;

	gtk_window_set_default_size (GTK_WINDOW (new),
				     DEFAULT_WIDTH, DEFAULT_HEIGHT);

	new->menu_handler = explorer_window_menu_handler_new (new);

	/* Finally, set up the GUI.  */

	update_gui_controls (new);
	explorer_window_set_directory_view_mode
		(new, EXPLORER_DIRECTORY_VIEW_MODE_ICONS);

	return GTK_WIDGET (new);
}


const GnomeVFSURI *
explorer_window_get_current_uri (ExplorerWindow *window)
{
	g_return_val_if_fail (window != NULL, NULL);
	g_return_val_if_fail (EXPLORER_IS_WINDOW (window), NULL);

	return window->uri;
}

/**
 * explorer_window_load_directory:
 * @window: An ExplorerWindow widget.
 * @uri: URI to be loaded into @window.
 * 
 * Load @uri (which must point to a directory) in @window.
 **/
gboolean
explorer_window_load_directory (ExplorerWindow *window,
				const GnomeVFSURI *uri)
{
	GtkWidget *directory_view;

	g_return_val_if_fail (window != NULL, FALSE);
	g_return_val_if_fail (EXPLORER_IS_WINDOW (window), FALSE);
	g_return_val_if_fail (uri != NULL, FALSE);

	/* FIXME stop an existing load in progress.  */

	if (window->new_uri != NULL)
		gnome_vfs_uri_unref (window->new_uri);

	/* KLUDGE ALERT!  This makes sure we have a trailing `/' for all the
           directories we open.  */
	window->new_uri = gnome_vfs_uri_append_path (uri, "");

	start_open (window);

	directory_view
		= explorer_directory_view_new (window->application,
					       GNOME_APPBAR (window->app_bar),
					       window->directory_view_mode);

	gtk_signal_connect (GTK_OBJECT (directory_view), "open_failed",
			    GTK_SIGNAL_FUNC (directory_open_failed_cb),
			    window);
	gtk_signal_connect (GTK_OBJECT (directory_view), "open_done",
			    GTK_SIGNAL_FUNC (directory_open_done_cb),
			    window);
	gtk_signal_connect (GTK_OBJECT (directory_view), "load_failed",
			    GTK_SIGNAL_FUNC (directory_load_failed_cb),
			    window);
	gtk_signal_connect (GTK_OBJECT (directory_view), "load_done",
			    GTK_SIGNAL_FUNC (directory_load_done_cb),
			    window);

	window->directory_view = EXPLORER_DIRECTORY_VIEW (directory_view);
	explorer_directory_view_load_uri (window->directory_view,
					  window->new_uri);

	return FALSE;
}


/* Implementation of loading through embedded Bonobo components and
   ExplorerLoader.  */

static GnomeViewFrame *
new_view_frame_for_loader (ExplorerApplication *application,
			   ExplorerLoader *loader)
{
	ExplorerViewManager *view_manager;
	const gchar *mime_type;

	view_manager = explorer_application_get_view_manager (application);
	mime_type = explorer_loader_get_mime_type (loader);

	return explorer_view_manager_new_view_for_mime_type (view_manager,
							     mime_type);
}

static gboolean
do_load (ExplorerWindow *window,
	 ExplorerLoader *loader,
	 GnomeViewFrame *view_frame)
{
	GtkWidget *wrapper_widget;
	GnomeObjectClient *object_client;
	GnomeClientSite *client_site;

	if (window->new_uri != NULL)
		gnome_vfs_uri_unref (window->new_uri);
	window->new_uri = gnome_vfs_uri_dup (explorer_loader_get_uri (loader));

	if (window->loader != NULL) {
		gtk_object_destroy (GTK_OBJECT (window->loader));
		window->loader = NULL;
	}

	start_load (window);

	gnome_view_frame_set_covered (view_frame, FALSE);

	wrapper_widget = gnome_view_frame_get_wrapper (view_frame);

	set_contents (window, wrapper_widget);

	/* It is very important to show the window here.  Some components (such
           as the HTML component) cannot deal with fed data until they are
           realized.  */
	gtk_widget_show (GTK_WIDGET (window));

	client_site = gnome_view_frame_get_client_site (view_frame);
	object_client = gnome_client_site_get_embeddable (client_site);

	if (! explorer_loader_set_component (loader,
					     GNOME_OBJECT (object_client))) {
		error (GTK_WIDGET (window),
		       _("The component loaded for type `%s'\n"
			 "does not provide the required functionality."),
		       explorer_loader_get_mime_type (loader));
		return FALSE;
	}

	window->loader = loader;

	gtk_signal_connect (GTK_OBJECT (loader), "load_done",
			    GTK_SIGNAL_FUNC (load_done_cb), window);

	gtk_signal_connect (GTK_OBJECT (loader), "status_message",
			    GTK_SIGNAL_FUNC (status_message_cb), window);
	
	return TRUE;
}

/**
 * explorer_window_set_loader:
 * @window: An ExplorerWindow.
 * @loader: An ExplorerLoader which has passed the `open' phase successfully
 * (i.e. it has already emitted the "open_done" signal).
 * 
 * Put an embeddable component in @window, filling it through @loader.  It
 * assumes that the @loader is already past the `open' phase: this function is
 * used to load a file into an embeddable component after it has been open and
 * the MIME type has been discovered.
 *
 * This will also show the window if successful.
 *
 * Return value: %TRUE if successfull, %FALSE otherwise.
 **/
gboolean
explorer_window_set_loader (ExplorerWindow *window,
			    ExplorerLoader *loader)
{
	GnomeViewFrame *view_frame;

	g_return_val_if_fail (window != NULL, FALSE);
	g_return_val_if_fail (EXPLORER_IS_WINDOW (window), FALSE);
	g_return_val_if_fail (loader != NULL, FALSE);
	g_return_val_if_fail (EXPLORER_IS_LOADER (loader), FALSE);

	/* FIXME stop current loading.  */
	/* FIXME check that the loader is past the `open' phase too.  */
	/* FIXME stop an existing load in progress.  */
	/* FIXME check for leaks.  I believe I am not freeing all the Bonobo
           stuff correctly.  */

	EXPLORER_DEBUG (("Entering function"));

	view_frame = new_view_frame_for_loader (window->application, loader);
	if (view_frame == NULL)
		return FALSE;

	return do_load (window, loader, view_frame);
}

/**
 * explorer_window_load_from_loader:
 * @application: An ExplorerApplication.
 * @loader: An ExplorerLoader which has passed the `open' phase successfully
 * (i.e. it has already emitted the "open_done" signal).
 * 
 * Create a new window and put an embeddable component in it, filling it
 * through @loader.  It assumes that the @loader is already past the `open'
 * phase: this function is used by the ExplorerApplication to load a file into
 * an embeddable component after it has been open and the MIME type has been
 * discovered.
 *
 * You should never call this function directly: use
 * %explorer_application_new_window_with_loader instead.
 * 
 * Return value: A pointer to the new ExplorerWindow widget.
 **/
GtkWidget *
explorer_window_new_with_loader (ExplorerApplication *application,
				 ExplorerLoader *loader)
{
	ExplorerWindow *window;
	GnomeViewFrame *view_frame;
	GtkWidget *window_widget;

	g_return_val_if_fail (application != NULL, FALSE);
	g_return_val_if_fail (loader != NULL, FALSE);
	g_return_val_if_fail (EXPLORER_IS_LOADER (loader), FALSE);

	/* FIXME check that the loader is past the `open' phase too.  */
	/* FIXME stop an existing load in progress.  */
	/* FIXME check for leaks.  I believe I am not freeing all the Bonobo
           stuff correctly.  */

	EXPLORER_DEBUG (("Entering function"));

	view_frame = new_view_frame_for_loader (application, loader);
	if (view_frame == NULL)
		return NULL;

	window_widget = explorer_window_new (application);
	window = EXPLORER_WINDOW (window_widget);

	if (! do_load (window, loader, view_frame)) {
		gtk_widget_destroy (window_widget);
		window_widget = NULL;
	}

	return window_widget;
}


/**
 * explorer_window_back:
 * @window: An ExplorerWindow.
 * 
 * Go back in the window's history.
 * 
 * Return value: %TRUE if successfull, %FALSE otherwise.
 **/
gboolean
explorer_window_back (ExplorerWindow *window)
{
	const ExplorerHistoryItem *item;

	g_return_val_if_fail (window != NULL, FALSE);
	g_return_val_if_fail (EXPLORER_IS_WINDOW (window), FALSE);

	if (! explorer_history_back (window->history))
		return FALSE;

	item = explorer_history_get_current (window->history);

	explorer_application_open (window->application, item->uri,
				   window, NULL, NULL,
				   EXPLORER_APPLICATION_OPEN_SAMEWINDOW);

	return TRUE;
}

/**
 * explorer_window_forward:
 * @window: An ExplorerWindow.
 * 
 * Go forward in the window's history.
 * 
 * Return value: %TRUE if successfull, %FALSE otherwise.
 **/
gboolean
explorer_window_forward (ExplorerWindow *window)
{
	const ExplorerHistoryItem *item;

	g_return_val_if_fail (window != NULL, FALSE);
	g_return_val_if_fail (EXPLORER_IS_WINDOW (window), FALSE);

	if (! explorer_history_forward (window->history))
		return FALSE;

	item = explorer_history_get_current (window->history);

	explorer_application_open (window->application, item->uri,
				   window, NULL, NULL,
				   EXPLORER_APPLICATION_OPEN_SAMEWINDOW);

	return TRUE;
}

/**
 * explorer_window_up_to_parent:
 * @window: An ExplorerWindow.
 * 
 * Open the parent of the URI currently shown in @window (which must be a
 * directory).
 * 
 * Return value: %TRUE if successfull, %FALSE otherwise.
 **/
gboolean 
explorer_window_up_to_parent (ExplorerWindow *window)
{
	GnomeVFSURI *new_uri;

	g_return_val_if_fail (window != NULL, FALSE);
	g_return_val_if_fail (EXPLORER_IS_WINDOW (window), FALSE);

	if (window->uri == NULL)
		return FALSE;

	new_uri = gnome_vfs_uri_get_parent (window->uri);

	explorer_application_open (window->application, new_uri,
				   window, "special/directory", NULL,
				   EXPLORER_APPLICATION_OPEN_DEFAULT);

	gnome_vfs_uri_unref (new_uri);

	return TRUE;
}

/**
 * explorer_window_home:
 * @window: An ExplorerWindow.
 * 
 * Open the user's home directory.
 * 
 * Return value: %TRUE if successfull, %FALSE otherwise.
 **/
gboolean
explorer_window_home (ExplorerWindow *window)
{
	GnomeVFSURI *new_uri;

	g_return_val_if_fail (window != NULL, FALSE);
	g_return_val_if_fail (EXPLORER_IS_WINDOW (window), FALSE);

	new_uri = gnome_vfs_uri_new (g_get_home_dir ());
	if (new_uri == NULL)
		return FALSE;

	explorer_application_open (window->application, new_uri,
				   window, "special/directory", NULL,
				   EXPLORER_APPLICATION_OPEN_DEFAULT);
	gnome_vfs_uri_unref (new_uri);

	return TRUE;
}

/**
 * explorer_window_reload:
 * @window: An ExplorerWindow.
 * 
 * Reload @window's contents.
 * 
 * Return value: %TRUE if successfull, %FALSE otherwise.
 **/
gboolean
explorer_window_reload (ExplorerWindow *window)
{
	GnomeVFSURI *new_uri;

	g_return_val_if_fail (window != NULL, FALSE);
	g_return_val_if_fail (EXPLORER_IS_WINDOW (window), FALSE);

	if (window->uri == NULL)
		return FALSE;

	new_uri = gnome_vfs_uri_dup (window->uri);

	/* FIXME TODO This is a bit tricky, because we don't want to use
	   `explorer_application_open()' and its layout heuristic.  So this
	   must be hacked up a bit.  */

	gnome_vfs_uri_unref (new_uri);

	return TRUE;
}

/**
 * explorer_window_stop:
 * @window: An ExplorerWindow.
 * 
 * Stop @window's current transfer.
 * 
 * Return value: %TRUE if successfull, %FALSE otherwise.
 **/
gboolean
explorer_window_stop (ExplorerWindow *window)
{
	g_return_val_if_fail (window != NULL, FALSE);
	g_return_val_if_fail (EXPLORER_IS_WINDOW (window), FALSE);

	if (window->directory_view != NULL)
		explorer_directory_view_stop (window->directory_view);

	if (window->loader != NULL) {
		gtk_object_destroy (GTK_OBJECT (window->loader));
		window->loader = NULL;
	}

	explorer_window_toolbar_allow_stop (window->toolbar, FALSE);

	return TRUE;
}


/**
 * explorer_window_set_directory_view_mode:
 * @window: An ExplorerWindow.
 * @mode: An integer representing the directory view mode.
 * 
 * Set directory view mode for @window.
 **/
void
explorer_window_set_directory_view_mode (ExplorerWindow *window,
					 ExplorerDirectoryViewMode mode)
{
	g_return_if_fail (window != NULL);
	g_return_if_fail (EXPLORER_IS_WINDOW (window));
	g_return_if_fail (explorer_directory_view_is_valid_mode (mode));

	if (window->directory_view_mode == mode)
		return;

	window->directory_view_mode = mode;

	if (window->directory_view != NULL)
		explorer_directory_view_set_mode (window->directory_view, mode);
}


/**
 * explorer_window_get_layout:
 * @window: An ExplorerWindow.
 * 
 * Retrieve the current icon layout from @window.
 * 
 * Return value: A pointer to the layout object.
 **/
ExplorerWindowLayout *
explorer_window_get_layout (ExplorerWindow *window)
{
	g_return_val_if_fail (window != NULL, NULL);
	g_return_val_if_fail (EXPLORER_IS_WINDOW (window), NULL);

	return explorer_window_layout_new_from_window (window);
}

/**
 * explorer_window_line_up_icons:
 * @window: An ExplorerWindow.
 * 
 * Line up icons in @window.
 **/
void
explorer_window_line_up_icons (ExplorerWindow *window)
{
	g_return_if_fail (window != NULL);
	g_return_if_fail (EXPLORER_IS_WINDOW (window));

	if (window->directory_view == NULL)
		return;

	explorer_directory_view_line_up_icons (window->directory_view);
}


/**
 * explorer_window_sort:
 * @window: An ExplorerWindow.
 * @sort_type: An integer representing the sorting method.
 * 
 * Sort items in @window.
 **/
void
explorer_window_sort (ExplorerWindow *window,
		      ExplorerDirectoryViewSortType sort_type)
{
	g_return_if_fail (window != NULL);
	g_return_if_fail (EXPLORER_IS_WINDOW (window));

	EXPLORER_DEBUG (("Entering function."));

	if (window->directory_view == NULL)
		return;

	explorer_directory_view_sort (window->directory_view, sort_type);
}
