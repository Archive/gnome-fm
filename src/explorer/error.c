/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* error.c - Error dialog for the GNOME Desktop File Operation Service.

   Copyright (C) 1999 Free Software Foundation

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public
   License along with this program; see the file COPYING.  If not,
   write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.

   Author: Ettore Perazzoli <ettore@gnu.org>
*/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gnome.h>

#include "error.h"


struct _ErrorInfo {
	ErrorDismissCallback callback;
	gpointer callback_data;
	gboolean closed;
};
typedef struct _ErrorInfo ErrorInfo;

static void
error_dialog_callback  (GnomeDialog *dialog,
			gpointer data)
{
	ErrorInfo *error_info;

	error_info = (ErrorInfo *) data;

	if (error_info->callback != NULL)
		(* error_info->callback) (error_info->callback_data);

	error_info->closed = TRUE;
}


static void
error_internal (GtkWidget *parent,
		ErrorDismissCallback callback,
		gpointer callback_data,
		const gchar *format,
		va_list ap)
{
	ErrorInfo *error_info;
	GtkWidget *dialog;
	gchar *s;

	error_info = g_new (ErrorInfo, 1);
	error_info->callback = callback;
	error_info->callback_data = callback_data;
	error_info->closed = FALSE;

	s = g_strdup_vprintf (format, ap);

	dialog = gnome_error_dialog_parented (s, GTK_WINDOW(parent));
	gtk_signal_connect (GTK_OBJECT (dialog), "close",
			    GTK_SIGNAL_FUNC (error_dialog_callback),
			    error_info);

	g_free (s);

	while (!error_info->closed)
		g_main_iteration(TRUE);

	g_free (error_info);
}


void
error (GtkWidget *parent,
       const gchar *format,
       ...)
{
	va_list ap;

	va_start (ap, format);

	error_internal (parent, NULL, NULL, format, ap);

	va_end (format);
}

void
error_with_callback (GtkWidget *parent,
		     ErrorDismissCallback callback,
		     gpointer callback_data,
		     const gchar *format,
		     ...)
{
	va_list ap;

	va_start (ap, format);

	error_internal (parent, callback, callback_data, format, ap);

	va_end (format);
}


