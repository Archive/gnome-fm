/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* explorer-view-progressive-loader.h
 *
 * Copyright (C) 1999  Free Software Foundaton
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Author: Ettore Perazzoli
 */

/* This file provides the GnomeProgressiveLoader-derived object used to feed
   the explorer views with additional data that they might need (for example,
   extra URIs for inline contents in HTML pages).  */

#ifndef __EXPLORER_VIEW_PROGRESSIVE_LOADER_H__
#define __EXPLORER_VIEW_PROGRESSIVE_LOADER_H__

#include <gnome.h>

#include "gnome-progressive-loader.h"

#ifdef __cplusplus
extern "C" {
#pragma }
#endif /* __cplusplus */

#define EXPLORER_TYPE_VIEW_PROGRESSIVE_LOADER \
	(explorer_view_progressive_loader_get_type ())
#define EXPLORER_VIEW_PROGRESSIVE_LOADER(obj) \
	(GTK_CHECK_CAST ((obj), EXPLORER_TYPE_VIEW_PROGRESSIVE_LOADER, ExplorerViewProgressiveLoader))
#define EXPLORER_VIEW_PROGRESSIVE_LOADER_CLASS(klass) \
	(GTK_CHECK_CLASS_CAST ((klass), EXPLORER_TYPE_VIEW_PROGRESSIVE_LOADER, ExplorerViewProgressiveLoaderClass))
#define EXPLORER_IS_VIEW_PROGRESSIVE_LOADER(obj) \
	(GTK_CHECK_TYPE ((obj), EXPLORER_TYPE_VIEW_PROGRESSIVE_LOADER))
#define EXPLORER_IS_VIEW_PROGRESSIVE_LOADER_CLASS(klass) \
	(GTK_CHECK_CLASS_TYPE ((obj), EXPLORER_TYPE_VIEW_PROGRESSIVE_LOADER))


typedef struct _ExplorerViewProgressiveLoader       ExplorerViewProgressiveLoader;
typedef struct _ExplorerViewProgressiveLoaderClass  ExplorerViewProgressiveLoaderClass;

struct _ExplorerViewProgressiveLoader {
	GnomeProgressiveLoader parent;

	GList *loaders;
};

struct _ExplorerViewProgressiveLoaderClass {
	GnomeProgressiveLoaderClass parent_class;
};


GtkType explorer_view_progressive_loader_get_type (void);
void explorer_view_progressive_loader_construct (ExplorerViewProgressiveLoader *loader);
ExplorerViewProgressiveLoader *explorer_view_progressive_loader_new (void);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __EXPLORER_VIEW_PROGRESSIVE_LOADER_H__ */
