/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* explorer-window-menu-handler.c
 *
 * Copyright (C) 1999  Free Software Foundaton
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Author: Ettore Perazzoli
 */

#include <gnome.h>

#include "explorer-window-menu-handler.h"


static void
file_close_cb (GtkWidget *widget,
	       gpointer data)
{
	GtkWidget *window;

	window = GTK_WIDGET (data);
	gtk_widget_destroy(window);
}

static void
file_exit_cb (GtkWidget *widget,
	       gpointer data)
{
	gtk_main_quit();
}

static void
view_icons_cb (GtkWidget *widget,
	       gpointer data)
{
	ExplorerWindow *window;

	window = EXPLORER_WINDOW (data);
	explorer_window_set_directory_view_mode
		(window, EXPLORER_DIRECTORY_VIEW_MODE_ICONS);
}

static void
view_small_icons_cb (GtkWidget *widget,
		     gpointer data)
{
	ExplorerWindow *window;

	window = EXPLORER_WINDOW (data);
	explorer_window_set_directory_view_mode
		(window, EXPLORER_DIRECTORY_VIEW_MODE_SMALLICONS);
}

static void
view_detailed_cb (GtkWidget *widget,
		     gpointer data)
{
	ExplorerWindow *window;

	window = EXPLORER_WINDOW (data);
	explorer_window_set_directory_view_mode
		(window, EXPLORER_DIRECTORY_VIEW_MODE_DETAILED);
}

static void
view_custom_cb (GtkWidget *widget,
		gpointer data)
{
	ExplorerWindow *window;

	window = EXPLORER_WINDOW (data);
	explorer_window_set_directory_view_mode
		(window, EXPLORER_DIRECTORY_VIEW_MODE_CUSTOM);
}


static GnomeUIInfo file_menu_info[] = {
	GNOMEUIINFO_MENU_CLOSE_ITEM(file_close_cb,NULL),
	GNOMEUIINFO_SEPARATOR,
	GNOMEUIINFO_MENU_EXIT_ITEM(file_exit_cb,NULL),
	GNOMEUIINFO_END
};

static GnomeUIInfo view_radio_list[] = {
	GNOMEUIINFO_RADIOITEM (N_("_Icons"),
			       N_("Show files and directories as icons"),
			       view_icons_cb,
			       NULL),
	GNOMEUIINFO_RADIOITEM (N_("_Small icons"),
			       N_("Show files and directories as small icons"),
			       view_small_icons_cb,
			       NULL),
	GNOMEUIINFO_RADIOITEM (N_("_Detailed"),
			       N_("Show files and directories as text with extended information"),
			       view_detailed_cb,
			       NULL),
	GNOMEUIINFO_RADIOITEM (N_("_Custom"),
			       N_("Show files and directories as text with user-specified information"),
			       view_custom_cb,
			       NULL),
	GNOMEUIINFO_END
};


static void
sort_by_name_cb (GtkWidget *widget,
		 gpointer data)
{
	ExplorerWindow *window;

	window = EXPLORER_WINDOW (data);
	explorer_window_sort (window, EXPLORER_DIRECTORY_VIEW_SORT_BYNAME);
}

static void
sort_by_size_cb (GtkWidget *widget,
		 gpointer data)
{
	ExplorerWindow *window;

	window = EXPLORER_WINDOW (data);
	explorer_window_sort (window, EXPLORER_DIRECTORY_VIEW_SORT_BYSIZE);
}

static GnomeUIInfo sort_subtree[] = {
	GNOMEUIINFO_ITEM_NONE (N_("by Name"),
			  N_("Order icons by name"),
			  sort_by_name_cb),
	GNOMEUIINFO_ITEM_NONE (N_("by Size"),
			  N_("Order icons by size"),
			  sort_by_size_cb),
	GNOMEUIINFO_END
};


static void
line_up_icons_cb (GtkWidget *widget,
		  gpointer data)
{
	ExplorerWindow *window;

	window = EXPLORER_WINDOW (data);
	explorer_window_line_up_icons (window);
}

static GnomeUIInfo view_menu_info[] = {
	GNOMEUIINFO_RADIOLIST (view_radio_list),
	GNOMEUIINFO_SEPARATOR,
	GNOMEUIINFO_ITEM_NONE (N_("_Line up icons"),
			  N_("Line up icons in the view"),
			  line_up_icons_cb),
	GNOMEUIINFO_SEPARATOR,
	GNOMEUIINFO_SUBTREE (N_("Sort"), sort_subtree),
	GNOMEUIINFO_END
};

static GnomeUIInfo menu_info[] = {
	GNOMEUIINFO_MENU_FILE_TREE (file_menu_info),
	GNOMEUIINFO_SUBTREE (N_("_View"), view_menu_info),
	GNOMEUIINFO_END
};


ExplorerWindowMenuHandler *
explorer_window_menu_handler_new (ExplorerWindow *window)
{
	ExplorerWindowMenuHandler *handler;

	handler = g_new (ExplorerWindowMenuHandler, 1);

	gnome_app_create_menus_with_data (GNOME_APP (window), menu_info, window);

	return handler;
}

void
explorer_window_menu_handler_destroy (ExplorerWindowMenuHandler *handler)
{
	g_return_if_fail (handler != NULL);

	g_free (handler);
}
