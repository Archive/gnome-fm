/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* main.c - Main program file for the GNOME Explorer program.

   Copyright (C) 1999 Free Software Foundation

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public
   License along with this program; see the file COPYING.  If not,
   write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.

   Author: Ettore Perazzoli <ettore@gnu.org> */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gnome.h>
#include <libgnorba/gnorba.h>

#include "explorer.h"


int
main (int argc, char **argv)
{
	ExplorerApplication *application;
	CORBA_Environment ev;
	GtkWidget *window;

	CORBA_exception_init (&ev);

	gnome_CORBA_init ("FileManager", VERSION, &argc, argv,
			  GNORBA_INIT_SERVER_FUNC,
			  &ev);
	if (ev._major != CORBA_NO_EXCEPTION)
		g_error (_("Cannot initialize CORBA."));

	g_thread_init (NULL);

	if (! bonobo_init (gnome_CORBA_ORB (), NULL, NULL))
		g_error (_("Cannot initialize Bonobo"));

	if (! gnome_vfs_init ())
		g_error (_("Cannot initialize GNOME VFS."));

	CORBA_exception_free (&ev);

	application = explorer_application_new ();
	if (application == NULL)
		g_error (_("Cannot create application instance."));

	window = explorer_application_new_window (application);

	if (argc < 2) {
		gtk_widget_show (window);
	} else {
		GnomeVFSURI *uri;

		uri = gnome_vfs_uri_new (argv[1]);
		explorer_application_open (application, uri,
					   EXPLORER_WINDOW (window), NULL, NULL,
					   EXPLORER_APPLICATION_OPEN_SAMEWINDOW);
		gnome_vfs_uri_unref (uri);
	}

	bonobo_main ();

	return 0;
}
