# Configure paths for GNOME VFS
# Ettore Perazzoli 99-08-19
# Chris Lahey	99-2-5
# stolen from Manish Singh again
# stolen back from Frank Belew
# stolen from Manish Singh
# Shamelessly stolen from Owen Taylor

dnl AM_PATH_GNOME_VFS([MINIMUM-VERSION, [ACTION-IF-FOUND [, ACTION-IF-NOT-FOUND]]])
dnl Test for GNOME VFS, and define GNOME_VFS_CFLAGS and GNOME_VFS_LIBS

AC_DEFUN(AM_PATH_GNOME_VFS,
[dnl 
dnl Get the cflags and libraries from the gnome-config script
dnl
AC_ARG_WITH(gnome-vfs-prefix,[  --with-gnome-vfs-prefix=PFX   Prefix where GNOME VFS is installed (optional)],
            gnome_vfs_prefix="$withval", gnome_vfs_prefix="")
AC_ARG_WITH(gnome-vfs-exec-prefix,[  --with-gnome-vfs-exec-prefix=PFX Exec prefix where GNOME VFS is installed (optional)],
            gnome_vfs_exec_prefix="$withval", gnome_vfs_exec_prefix="")
AC_ARG_ENABLE(gnome-vfstest, [  --disable-gnome-vfstest       Do not try to compile and run a test GNOME VFS program],
		    , enable_gnome_vfstest=yes)

  if test x$gnome_vfs_exec_prefix != x ; then
     gnome_vfs_args="$gnome_vfs_args --exec-prefix=$gnome_vfs_exec_prefix"
     if test x${GNOME_CONFIG+set} != xset ; then
        GNOME_CONFIG=$gnome_vfs_exec_prefix/bin/gnome-config
     fi
  fi
  if test x$gnome_vfs_prefix != x ; then
     gnome_vfs_args="$gnome_vfs_args --prefix=$gnome_vfs_prefix"
     if test x${GNOME_CONFIG+set} != xset ; then
        GNOME_CONFIG=$gnome_vfs_prefix/bin/gnome-config
     fi
  fi

  AC_PATH_PROG(GNOME_CONFIG, gnome-config, no)
  min_gnome_vfs_version=ifelse([$1], ,0.1.0,$1)
  AC_MSG_CHECKING(for GNOME VFS - version >= $min_gnome_vfs_version)
  no_gnome_vfs=""
  if test "$GNOME_CONFIG" = "no" ; then
    no_gnome_vfs=yes
  else
    GNOME_VFS_CFLAGS=`$GNOME_CONFIG $gnome_vfsconf_args --cflags vfspthread`
    GNOME_VFS_LIBS=`$GNOME_CONFIG $gnome_vfsconf_args --libs vfspthread`

    gnome_vfs_major_version=`$GNOME_CONFIG $gnome_vfs_args --version | \
           sed 's/\([[0-9]]*\).\([[0-9]]*\).\([[0-9]]*\)/\1/'`
    gnome_vfs_minor_version=`$GNOME_CONFIG $gnome_vfs_args --version | \
           sed 's/\([[0-9]]*\).\([[0-9]]*\).\([[0-9]]*\)/\2/'`
    gnome_vfs_micro_version=`$GNOME_CONFIG $gnome_vfs_config_args --version | \
           sed 's/\([[0-9]]*\).\([[0-9]]*\).\([[0-9]]*\)/\3/'`
    if test "x$enable_gnome_vfstest" = "xyes" ; then
      ac_save_CFLAGS="$CFLAGS"
      ac_save_LIBS="$LIBS"
      CFLAGS="$CFLAGS $GNOME_VFS_CFLAGS"
      LIBS="$LIBS $GNOME_VFS_LIBS"
dnl
dnl Now check if the installed GNOME VFS is sufficiently new. (Also sanity
dnl checks the results of gnome-config to some extent
dnl
      rm -f conf.gnome_vfstest
      AC_TRY_RUN([
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libgnomevfs/gnome-vfs.h>

static char*
my_strdup (char *str)
{
  char *new_str;
  
  if (str)
    {
      new_str = malloc ((strlen (str) + 1) * sizeof(char));
      strcpy (new_str, str);
    }
  else
    new_str = NULL;
  
  return new_str;
}

int main ()
{
  int major, minor, micro;
  char *tmp_version;

  system ("touch conf.gnome_vfstest");

  /* HP/UX 9 (%@#!) writes to sscanf strings */
  tmp_version = my_strdup("$min_gnome_vfs_version");
  if (sscanf(tmp_version, "%d.%d.%d", &major, &minor, &micro) != 3) {
     printf("%s, bad version string\n", "$min_gnome_vfs_version");
     exit(1);
   }
  return 0;
#if 0
   if (($gnome_vfs_major_version > major) ||
      (($gnome_vfs_major_version == major) && ($gnome_vfs_minor_version > minor)) ||
      (($gnome_vfs_major_version == major) && ($gnome_vfs_minor_version == minor) && ($gnome_vfs_micro_version >= micro)))
    {
      return 0;
    }
  else
    {
      printf("\n*** 'gnome-config vfs --version' returned %d.%d.%d, but the minimum version\n", $gnome_vfs_major_version, $gnome_vfs_minor_version, $gnome_vfs_micro_version);
      printf("*** of GNOME VFS required is %d.%d.%d. If gnome-config is correct, then it is\n", major, minor, micro);
      printf("*** best to upgrade to the required version.\n");
      printf("*** If gnome-config was wrong, set the environment variable GNOME_CONFIG\n");
      printf("*** to point to the correct copy of gnome-config, and remove the file\n");
      printf("*** config.cache before re-running configure\n");
      return 1;
    }
#endif
}

],, no_gnome_vfs=yes,[echo $ac_n "cross compiling; assumed OK... $ac_c"])
       CFLAGS="$ac_save_CFLAGS"
       LIBS="$ac_save_LIBS"
     fi
  fi
  if test "x$no_gnome_vfs" = x ; then
     AC_MSG_RESULT(yes)
     ifelse([$2], , :, [$2])     
  else
     AC_MSG_RESULT(no)
     if test "$GNOME_CONFIG" = "no" ; then
       echo "*** The gnome-config script installed by GNOME-LIBS could not be found"
       echo "*** If GNOME VFS was installed in PREFIX, make sure PREFIX/bin is in"
       echo "*** your path, or set the GNOME_CONFIG environment variable to the"
       echo "*** full path to gnome-config."
     else
       if test -f conf.gnome_vfstest ; then
        :
       else
          echo "*** Could not run GNOME VFS test program, checking why..."
          CFLAGS="$CFLAGS $GNOME_VFS_CFLAGS"
          LIBS="$LIBS $GNOME_VFS_LIBS"
          AC_TRY_LINK([
#include <stdio.h>
#include <libgnomevfs/gnome-vfs.h>
],      [ return 0; ],
        [ echo "*** The test program compiled, but did not run. This usually means"
          echo "*** that the run-time linker is not finding GNOME VFS or finding the wrong"
          echo "*** version of GNOME VFS. If it is not finding GNOME VFS, you'll need to set your"
          echo "*** LD_LIBRARY_PATH environment variable, or edit /etc/ld.so.conf to point"
          echo "*** to the installed location  Also, make sure you have run ldconfig if that"
          echo "*** is required on your system"
	  echo "***"
          echo "*** If you have an old version installed, it is best to remove it, although"
          echo "*** you may also be able to get things to work by modifying LD_LIBRARY_PATH"],
        [ echo "*** The test program failed to compile or link. See the file config.log for the"
          echo "*** exact error that occured. This usually means GNOME VFS was incorrectly installed"
          echo "*** or that you have moved GNOME VFS since it was installed. In the latter case, you"
          echo "*** may want to edit the gnome-config script: $GNOME_CONFIG" ])
          CFLAGS="$ac_save_CFLAGS"
          LIBS="$ac_save_LIBS"
       fi
     fi
     GNOME_VFS_CFLAGS=""
     GNOME_VFS_LIBS=""
     ifelse([$3], , :, [$3])
  fi
  AC_SUBST(GNOME_VFS_CFLAGS)
  AC_SUBST(GNOME_VFS_LIBS)
  rm -f conf.gnome_vfstest
])

AC_DEFUN([GNOME_VFS_CHECK], [
	AM_PATH_GNOME_VFS(0.0,,[AC_MSG_ERROR(GNOME VFS not found)])
])
